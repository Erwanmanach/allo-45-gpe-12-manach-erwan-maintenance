import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurConcepteur implements EventHandler<ActionEvent>{
    private Questionnaire questionnaire;
    private AppliAllo45 appli;

    public ControleurConcepteur(Questionnaire questionnaire, AppliAllo45 appli){
        this.appli = appli;
        this.questionnaire = appli.getQuestionnaire();
    }

    public ControleurConcepteur(AppliAllo45 appli){
        this.appli = appli;
        this.questionnaire = appli.getQuestionnaire();
    }


    @Override
    public void handle(ActionEvent event){
        Button button = (Button) (event.getSource());
        if (button.getText().contains("Nouvelle")|| button.getText().contains("ajouter")){
            String typeQuestion = this.appli.popUpChoixTypeQuestion();
            if(typeQuestion.length() != 0){
                String titreQuestion = this.appli.popUpCreationTitreQuestion();
                if(titreQuestion.length() != 0){
                    Question q = new Question(titreQuestion, "" + Character.toLowerCase(typeQuestion.charAt(0)), 1);
                    this.questionnaire.ajouterQuestion(q);
                    this.appli.setQuestionActuelle(q);
                    this.appli.menuChoixQuestion();
                    this.appli.miseAJourQuestionConcepteur();
                }
                  
            }
            

            //if (typeQuestion.equals("Note")){appli. }
            
        }

        if (button.getText().equals("Créer réponse")){
            this.appli.sauvegardeQuestion();
            this.appli.getQuestionActuelle().getReponsePossible().add(" ");
            this.appli.miseAJourTilePaneConcepteur();
            //question.addReponsePossible("");
            //this.appli.miseAJourAffichage();
        }

        if (button.getId().contains("valider questionnaire")){
            String res = this.appli.popUpValiderQuestionnaire();
            if(res.equals("OK")){
                this.appli.sauvegarder();
                this.appli.validerQuestionnaire();
                this.appli.modeSelection(); 
            }
            else{}
        }

        else if (button.getText().contains("Supprimer")){
            int num = this.questionnaire.numQ(this.appli.getQuestionActuelle());
            Question question = this.appli.getQuestionActuelle();
            try{
                String res = this.appli.popUpSupprimerQuestion();
                if(res.equals("OK")){
                    this.questionnaire.supprimerQuestion(question); 
                    this.appli.menuChoixQuestion();
                    if(num>0){
                    this.appli.setQuestionActuelle(this.questionnaire.get(num-1));
                    this.appli.setnumQuestion(this.appli.getQuestionnaire().indexOf(this.appli.getQuestionActuelle())+1);
                    this.appli.miseAJourQuestionConcepteur();
                    }else{
                        this.appli.setQuestionActuelle(this.questionnaire.get(0));
                        this.appli.setnumQuestion(this.appli.getQuestionnaire().indexOf(this.appli.getQuestionActuelle())+1);
                        this.appli.miseAJourQuestionConcepteur();
                    }
                }
                else{}  
            }
            catch(QuestionnotFind e){
                System.out.println("question n'existe pas");
            }
            
        }

        else if (button.getId().equals("valider la question")){
            this.appli.sauvegardeQuestion();
            Question question = this.appli.getQuestionActuelle();
            if(question.getEtat() != 3){
                question.validerQuestion();
                this.appli.menuChoixQuestion();
                this.appli.miseAJourQuestionConcepteur();
            }
        }
    
        if (button.getText().equals("Créer un sondage")){
            String entreprise = this.appli.popUpChoixEntreprise();
            if(entreprise.length() != 0){
                String panel = this.appli.popUpChoixPanel();
                if(panel.length() != 0){
                    String nom = this.appli.popUpChoixTitreQuestionnaire();
                    this.appli.setQuestionnaire(new Questionnaire(nom,entreprise,panel));
                    this.appli.setEntreprise(entreprise);
                    this.appli.setPanel(panel);
                    this.appli.menuChoixQuestion();
                    this.appli.modeConcepteur();
                    this.appli.setQuestionActuelle(null);
                    this.appli.creer1ereReponse();
                    while(this.appli.getQuestionActuelle()==null){
                        String typeQuestion = this.appli.popUpChoixTypeQuestion();
                        if(typeQuestion.length() != 0){
                            String titreQuestion = this.appli.popUpCreationTitreQuestion();
                            if(titreQuestion.length() != 0){
                                Question q = new Question(titreQuestion, "" + Character.toLowerCase(typeQuestion.charAt(0)), 1);
                                Questionnaire questionnaire = this.appli.getQuestionnaire();
                                questionnaire.ajouterQuestion(q);
                                this.appli.setQuestionActuelle(q);
                                this.appli.menuChoixQuestion();
                                this.appli.miseAJourQuestionConcepteur();
                            }
                            
                        }
                    }
                }
                  
            }
            
        }
        if (button.getText().equals("Créer une entreprise")){
        this.appli.modeCreeEntreprise();
        
        
        }


        
    }
}
