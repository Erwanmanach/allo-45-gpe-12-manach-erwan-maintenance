import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurValiderEntreprise implements EventHandler<ActionEvent> {
    private AppliAllo45 appli;

    public ControleurValiderEntreprise(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        boolean valide=true;
        if(this.appli.getRS().getText().equals("")){
            this.appli.getRS().setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            valide = false;
        }else{
            this.appli.getRS().setStyle(null);
        }
        if(this.appli.getAD1().getText().equals("")){
            this.appli.getAD1().setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            valide = false;
        }else{
            this.appli.getAD1().setStyle(null);
        }
        if(this.appli.getV().getText().equals("")){
            this.appli.getV().setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            valide = false;
        }else{
            this.appli.getV().setStyle(null);
        }
        if(this.appli.getEmail().getText().equals("")){
            this.appli.getEmail().setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            valide = false;
        }else{
            this.appli.getEmail().setStyle(null);
        }
        if(this.appli.getTel().getText().equals("")){
            this.appli.getTel().setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            valide = false;
        }else{
            this.appli.getTel().setStyle(null);
        }
        try{
            Integer.parseInt(this.appli.getCP().getText());
            this.appli.getCP().setStyle(null);
        }catch(NumberFormatException e){
            this.appli.getCP().setStyle("-fx-border-color: red; -fx-border-width: 2px;");
            valide = false;
        }
        if(valide){
            this.appli.sauvegarderEntreprise();
            this.appli.modeSelection();
        }
    }
}