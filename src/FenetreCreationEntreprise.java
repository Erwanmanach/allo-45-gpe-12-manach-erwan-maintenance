import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class FenetreCreationEntreprise extends BorderPane {

    private AppliAllo45 appli;

    public FenetreCreationEntreprise(AppliAllo45 appli){ 
        super();
        
        this.appli = appli;
        this.setTop(this.banniere());
        this.setCenter(this.center());
        this.setBottom(this.bottom());
        this.setStyle("-fx-padding: 10;");
    }

    private BorderPane center(){ // réponse unique (text area)
        BorderPane centre = new BorderPane();
        GridPane champEtLabel = new GridPane();

        Label label1 = new Label("Raison sociale");
        champEtLabel.add(label1, 0, 0);

        champEtLabel.add(this.appli.getRS(), 1, 0);
 
        Label label2 = new Label("Adresse 1");
        champEtLabel.add(label2, 0, 1);

        champEtLabel.add(this.appli.getAD1(), 1, 1);


        Label label3 = new Label("Adresse 2");
        champEtLabel.add(label3, 0, 2);

        champEtLabel.add(this.appli.getAD2(), 1, 2);

        Label label4 = new Label("Code postal");
        champEtLabel.add(label4, 0, 3);

        champEtLabel.add(this.appli.getCP(), 1, 3);

        Label label5 = new Label("Ville");
        champEtLabel.add(label5, 0, 4);

        champEtLabel.add(this.appli.getV(), 1, 4);

        Label label6 = new Label("Télephone");
        champEtLabel.add(label6, 0, 5);

        champEtLabel.add(this.appli.getTel(), 1, 5);

        Label label7 = new Label("E-mail");
        champEtLabel.add(label7, 0, 6);

        champEtLabel.add(this.appli.getEmail(), 1, 6);

        centre.setCenter(champEtLabel);
        return centre;
    }


    //top left bottom right

    private BorderPane banniere(){
        BorderPane banniere = new BorderPane();
        banniere.setPadding(new Insets(0, 0, 20, 0));
        banniere.setRight(this.appli.getButtonDeconnexion());
        banniere.setStyle("-fx-margin: 100px;");
        return banniere;
    }

    private HBox bottom(){
        HBox bas = new HBox(30);
        bas.setPadding(new Insets(20, 0, 0, 0));
        ImageView Questionnaire = new ImageView(new Image("validerQuestionnaire.png"));
        Questionnaire.setFitWidth(35);
        Questionnaire.setFitHeight(35);
        Button validerQuestionnaire = new Button("Valider \nEntreprise", Questionnaire);
        validerQuestionnaire.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        validerQuestionnaire.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        validerQuestionnaire.setTextFill(Color.web("#137b8b"));
        validerQuestionnaire.setId("valider questionnaire");
        validerQuestionnaire.setOnAction(new ControleurValiderEntreprise(appli));
        Image home = new Image("home.png");
        ImageView maison = new ImageView(home);
        maison.setFitHeight(35);
        maison.setFitWidth(35);
        Button boutonRetour = new Button("Pour retourner à la \nséléction des sondages",maison);
        boutonRetour.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        boutonRetour.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        boutonRetour.setTextFill(Color.web("#137b8b"));
        boutonRetour.setContentDisplay(ContentDisplay.RIGHT);        
        boutonRetour.setPrefWidth(250);
        boutonRetour.setOnAction(new ControleurRetour(this.appli));
        bas.getChildren().addAll(validerQuestionnaire,boutonRetour);
        return bas;
    }
}