import java.sql.*;

public class BDSondeur {
	ConnexionMySQL laConnexion;
	Statement st;
    AppliAllo45 appli;
    public BDSondeur(ConnexionMySQL laConnexion,AppliAllo45 appli)throws SQLException{
        this.laConnexion = laConnexion;
        this.appli = appli;
        st = laConnexion.createStatement();
    }

    public Sonde selectSonde(Questionnaire questionnaire) throws SQLException{
        ResultSet reponse = st.executeQuery("Select Count(*) as nb from SONDE natural join CONSTITUER natural join QUESTIONNAIRE where QUESTIONNAIRE.idQ="+questionnaire.getId()+" and SONDE.numSond not in(SELECT numSond from INTERROGER where idQ="+questionnaire.getId()+")");
        reponse.next();
        if(reponse.getInt(1) > 0){
            ResultSet sonder = st.executeQuery("Select numSond, nomSond, prenomSond, telephoneSond, idC from SONDE natural join CONSTITUER natural join QUESTIONNAIRE where QUESTIONNAIRE.idQ="+questionnaire.getId()+" and SONDE.numSond not in(SELECT numSond from INTERROGER where idQ="+questionnaire.getId()+")");
            sonder.next();
            return new Sonde(sonder.getString("nomSond"), sonder.getString("prenomSond"), sonder.getString("telephoneSond"), sonder.getString("idC"), sonder.getInt("numSond"));
        }
        else{
            st.executeQuery("UPDATE QUESTIONNAIRE set Etat='A' where idQ="+questionnaire.getId());
            this.appli.modeSelection();
            return new Sonde(null, null, null, null,0);
        }
    }

    public void ajouterReponse(Questionnaire questionnaire, Sonde sonder, Utilisateur util)throws SQLException{
        for(Question question: questionnaire.getQuestion()){
            Reponse rep = question.getRepondu().get(0);
            st.executeQuery("insert into REPONDRE(idQ,numQ,idC,valeur) values ("+questionnaire.getId()+","+question.getNumQ()+",'"+sonder.getIdc()+"','"+rep.getIntitule()+"')");
        }
        st.executeQuery("insert into INTERROGER(idU,numSond,idQ) values ("+util.getId()+","+sonder.getNumSonde()+","+questionnaire.getId()+")");
    }

}
