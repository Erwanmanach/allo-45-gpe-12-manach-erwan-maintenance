import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class FenetreSelection extends VBox {

    private AppliAllo45 appli;

    private Button boutonDeconnexion;

    public FenetreSelection(AppliAllo45 appli) { // Prendra le bouton deconnexion et une liste de liste de string en parametre
        this.appli = appli;
        this.boutonDeconnexion = appli.getButtonDeconnexion();

        this.setStyle("-fx-background-color: #f9f7f0;");
        this.setPadding(new Insets(50));
        // Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        // int height = (int)dimension.getHeight();
        // int width  = (int)dimension.getWidth();
        // this.setBackground(new Background(new BackgroundImage(new Image("fondSelection.png", width, height, false, true), null,null,null,null)));
        
        BorderPane top = banniere(appli.getRole()); // méthode à faire

        top.setStyle("-fx-padding: 10;" + 
                      "-fx-background-insets: 5;" + 
                      "-fx-background-radius: 20;" + 
                      "-fx-background-color: #137b8b;"+
                      "-fx-border-insets: 5;" + 
                      "-fx-border-width: 10px;" +
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: #b4dbdc;");

        GridPane nomColonnes = nomsColonnes();
        nomColonnes.setStyle("-fx-background-color: #137b8b;"+
                      "-fx-border-color: #b4dbdc;");
        

        VBox contenu = contenu();
        ScrollPane listeDeroulante = new ScrollPane(contenu);
        listeDeroulante.setFitToWidth(true);

        VBox tableau = new VBox(nomColonnes, listeDeroulante);
        this.setSpacing(20);

        if (appli.getRole().contains("Concep")){  // méthiode à faire
            Button boutonSondage = new Button("Créer un sondage");
            Button boutonEntreprise = new Button("Créer une entreprise");
            boutonSondage.setId("new sondage");
            boutonEntreprise.setId("new entreprise");

            boutonSondage.setStyle("-fx-background-color: #fffbf0; -fx-border-color: #137b8b; -fx-border-width: 5px ; -fx-border-radius: 5;");
            boutonSondage.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 18.0));
            boutonSondage.setTextFill(Color.web("#137b8b"));
            boutonSondage.setContentDisplay(ContentDisplay.RIGHT);
            boutonSondage.setOnAction(new ControleurConcepteur(appli));

            boutonEntreprise.setStyle("-fx-background-color: #fffbf0; -fx-border-color: #137b8b; -fx-border-width: 5px ; -fx-border-radius: 5;");
            boutonEntreprise.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 18.0));
            boutonEntreprise.setTextFill(Color.web("#137b8b"));
            boutonEntreprise.setContentDisplay(ContentDisplay.RIGHT);
            boutonEntreprise.setOnAction(new ControleurConcepteur(appli));
    
            HBox boutonCreation = new HBox(boutonSondage,boutonEntreprise);
            boutonCreation.setSpacing(20);
            


            
            boutonCreation.setAlignment(Pos.TOP_RIGHT);
            this.getChildren().addAll(top, tableau, boutonCreation);
        }
        else{
            this.getChildren().addAll(top, tableau);
        }

    }
    
    public VBox contenu(){
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(9);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(45.75);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(40);
        VBox tableau = new VBox();
        //Pour récuperer la liste des sondages
        List<List<String>> listeSondage = null;
        try{
            if(appli.getRole().equals("Entreprise")){
                listeSondage = this.appli.getConnexionBD().listeSondageEntreprise(appli.getNumC());
            }else{
                listeSondage = this.appli.getConnexionBD().listeSondage(appli.getRole());
            }
        }catch(Exception e){}
        for (int i = 0; i < listeSondage.size(); i++) {
            GridPane ligne = new GridPane();
            ligne.setStyle("-fx-background-color: #f9f7f0");
            ligne.getColumnConstraints().addAll(column1, column2, column3);

            HBox numero = new HBox(new Label(""+(i+1)));
            numero.setPrefHeight(60);
            numero.setStyle("-fx-background-color: #f9f7f0;-fx-padding: 1 10 1 10; -fx-border-style: hidden solid hidden hidden;-fx-border-color: #b4dbdc; -fx-border-width:2px;");//quadrillage du numéro
            numero.setAlignment(Pos.CENTER_LEFT);

            HBox titre = new HBox(new Label(listeSondage.get(i).get(0)));
            titre.setStyle("-fx-padding: 1 10 1 10; -fx-border-style: hidden solid hidden solid;-fx-border-color: #b4dbdc; -fx-border-width:2px;");//quadrillage du titre
            titre.setPrefHeight(60);           
            titre.setAlignment(Pos.CENTER_LEFT);

            
            HBox panel = new HBox(new Label(listeSondage.get(i).get(1)));
            panel.setStyle("-fx-padding: 1 10 1 10; -fx-border-style: hidden hidden hidden solid;-fx-border-color: #b4dbdc; -fx-border-width:2px;");
            panel.setPrefHeight(60);
            panel.setAlignment(Pos.CENTER_LEFT);

            ligne.add(numero, 0, i);
            ligne.add(titre, 1, i);
            ligne.add(panel, 2, i);
            
            Button bouton = new Button("",ligne);
            bouton.setId(listeSondage.get(i).get(2));
            bouton.setOnAction(new ControleurQuestionnaire(appli, appli.getBdQuestionnaire()));
            bouton.setStyle("-fx-padding: 0 0 0 10; -fx-border-style: solid; -fx-border-color: #b4dbdc; -fx-border-width:2px;");
            bouton.setMaxWidth(Double.MAX_VALUE);


            tableau.getChildren().add(bouton);
        }
        tableau.setStyle("-fx-background-color: #f9f7f0;");
        return tableau;
    }

    private GridPane nomsColonnes(){
        GridPane nomColonnes = new GridPane();
        Text num = new Text("n°");
        HBox nomCol = new HBox(num);
        nomColonnes.add(nomCol, 0, 0);
        num.setStyle("-fx-font-weight:bold;-fx-font-size:1.2em;-fx-padding:4px;");
        num.setFill(Color.web("#fffbf0")); 
        nomCol.setStyle("-fx-background-color: #137b8b; -fx-padding: 10; -fx-border-width: 3px; -fx-border-style: solid; -fx-border-color: #b4dbdc;");

        Text sond = new Text("Sondage");
        HBox sondCol = new HBox(sond);
        nomColonnes.add(sondCol, 1, 0);
        sond.setStyle("-fx-font-weight:bold;-fx-font-size:1.2em;-fx-padding:4px;");
        sond.setFill(Color.web("#fffbf0")); 
        sondCol.setStyle("-fx-background-color: #137b8b; -fx-padding: 10; -fx-border-width: 3px;  -fx-border-style: solid; -fx-border-color: #b4dbdc;");

        Text pan = new Text("Panel");
        HBox panelCol = new HBox(pan);
        nomColonnes.add(panelCol, 2, 0);
        pan.setStyle("-fx-font-weight:bold;-fx-font-size:1.2em;-fx-padding:4px;");
        pan.setFill(Color.web("#fffbf0")); 
        panelCol.setStyle("-fx-background-color: #137b8b; -fx-padding: 10; -fx-border-width: 3px;  -fx-border-style: solid; -fx-border-color: #b4dbdc;");
        
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(10);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(45);
        nomColonnes.getColumnConstraints().addAll(column1, column2, column2);

        return nomColonnes;
    }

    private BorderPane banniere(String module){        
        BorderPane ban = new BorderPane();
        ImageView deco = new ImageView("deconnexion.png");
        deco.setFitHeight(15);
        deco.setFitWidth(15);
        
        ImageView iconDeconnexion = new ImageView(new Image("deconnexion.png"));
        iconDeconnexion.setFitHeight(40);
        iconDeconnexion.setFitWidth(40);

        this.boutonDeconnexion= new Button("Deconnexion ",iconDeconnexion);
        this.boutonDeconnexion.setStyle("-fx-background-color: #fffbf0;");
        this.boutonDeconnexion.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 18.0));
        this.boutonDeconnexion.setTextFill(Color.web("#137b8b"));
        this.boutonDeconnexion.setContentDisplay(ContentDisplay.RIGHT);

        
        this.boutonDeconnexion.setOnAction(new ControleurConnexion(appli,appli.getConnexionBD()));
        Text consigne = new Text("Selectionnez votre sondage");
        consigne.setStyle("-fx-font-weight:bold;-fx-font-size:2em;-fx-padding:4px;");
        consigne.setFill(Color.web("#fffbf0")); 
        ban.setCenter(consigne);
        Text mod = new Text("Module\n"+module);
        mod.setTextAlignment(TextAlignment.CENTER);
        mod.setStyle("-fx-font-weight:bold;-fx-font-size:1.5em;-fx-padding:4px;");
        mod.setFill(Color.web("#fffbf0")); 

        ban.setLeft(mod);
        ban.setRight(boutonDeconnexion);
        ban.setStyle("-fx-font-size: 1.2em;-fx-font-color:#072A40;-fx-background-color: #178ca4; -fx-padding: 10; -fx-border-style: solid;");
        return ban;
    }
}