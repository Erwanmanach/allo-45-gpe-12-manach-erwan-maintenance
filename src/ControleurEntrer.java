import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import java.sql.SQLException;
import javafx.event.EventHandler;

public class ControleurEntrer implements EventHandler<KeyEvent>{
    private AppliAllo45 appli;
    private ConnexionBD connexionBD;
    
    public ControleurEntrer(AppliAllo45 appli,ConnexionBD connexionBD){
        this.appli = appli;
        this.connexionBD = connexionBD;
    }
    
    public void handle(KeyEvent e){
        if (e.getCode().equals(KeyCode.ENTER)){
            String login = this.appli.getTextLogin();
            String mdp = this.appli.getTextMotDePasse();
            if (appli.verifieIdentifiant()){
                this.appli.afficherMessageMauvaisIdentifiant();
            }
            else{
                try{
                    Utilisateur user = this.connexionBD.connexion(login, mdp);
                    if (user == null){
                        this.appli.afficherMessageMauvaisIdentifiant();// Mettre une popup ou un texte en rouge
                    }
                    else{
                        this.appli.setUtilisateur(user);
                        this.appli.modeSelection();
                    }
                }
                catch (SQLException ex){
                    this.appli.afficherMessageErreurConnexion();
                }
            }
        }
    }
}
