import java.sql.*;

public class ConnexionMySQL {
	private Connection mysql=null;
	private boolean connecte=false;
	public ConnexionMySQL() throws ClassNotFoundException{
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		}catch(ClassNotFoundException ex){
			System.out.println("Msg : "+ex.getMessage());
		}
	}

	public void connecter() throws SQLException {
		try{
			mysql=DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3306/sondage", "root", "mdp_root");
		}
		// si tout c'est bien passé la connexion n'est plus nulle
		catch(SQLException ex){
			System.out.println("Msg : "+ex.getMessage()+ex.getErrorCode()) ;
		}
		this.connecte=this.mysql!=null;
	}
	public void close() throws SQLException {
		// fermer la connexion
		if (mysql != null){
			try {
				mysql.close();
			}catch(SQLException ex){
				System.out.println("Msg : "+ex.getMessage()+ex.getErrorCode());
			}
		}
		this.connecte=false;
	}

    public boolean isConnecte() { return this.connecte;}
	public Statement createStatement() throws SQLException {
		return this.mysql.createStatement();
	}

	public PreparedStatement prepareStatement(String requete) throws SQLException{
		return this.mysql.prepareStatement(requete);
	}
	
}
