import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurReponse implements EventHandler<ActionEvent> {
    private AppliAllo45 appli;

    public ControleurReponse(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        try{
            this.appli.getBDSondeur().ajouterReponse(this.appli.getQuestionnaire(),this.appli.getSonde(), this.appli.getUtilisateur());
        }catch(SQLException sqle){
            System.out.println(sqle);
        }
        this.appli.modeSelection();
    }
}