import java.sql.*;

public class BDQuestionnaire {
	ConnexionMySQL laConnexion;
	Statement st;
    public BDQuestionnaire(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }
    public Questionnaire getQuestionnaireConcepteur(String id)throws SQLException{
        st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("SELECT Titre, nomPan, idPan, raisonSoc, numC from QUESTIONNAIRE natural join PANEL natural join CLIENT where idQ="+id);
        rs.next();
        Questionnaire res = new Questionnaire(rs.getString("Titre"),rs.getString("raisonSoc"),rs.getString("nomPan"),Integer.parseInt(id));
        ResultSet rs2 = st.executeQuery("SELECT texteQ, numQ, IFNULL(MaxVal,1) as maxVal, idT from QUESTION where idQ='"+id+"' ORDER BY numQ");
        while(rs2.next()){
            System.out.println(rs2.getInt("numQ"));
            Question question = new Question(rs2.getString("texteQ"),rs2.getString("idT"),rs2.getInt("maxVal"),Integer.parseInt(id),rs2.getInt("numQ"));
            if(rs2.getString("idT").equals("u") || rs2.getString("idT").equals("m") || rs2.getString("idT").equals("c")){
                ResultSet reponse = st.executeQuery("Select Valeur from VALPOSSIBLE where numQ='"+rs2.getString("numQ")+"' AND idQ='"+id+"' ORDER BY idV");
                while(reponse.next()){
                    question.addReponsePossible(reponse.getString("Valeur"));
                }
            }
            res.ajouterQuestion(question);
        }
        return res;
    }
    public Questionnaire getQuestionnaireAnalyste(String id)throws SQLException{
        Statement st = laConnexion.createStatement();
        PreparedStatement ps = laConnexion.prepareStatement("SELECT Titre, nomPan, raisonSoc from QUESTIONNAIRE natural join PANEL natural join CLIENT where idQ=?");
        ps.setInt(1, Integer.parseInt(id));
        ResultSet rs = ps.executeQuery();
        rs.next();
        Questionnaire res = new Questionnaire(rs.getString("Titre"),rs.getString("raisonSoc"),rs.getString("nomPan"),Integer.parseInt(id));
        ResultSet rs2 = st.executeQuery("SELECT texteQ, numQ, IFNULL(MaxVal,1) as maxVal, idT, intituleT from QUESTION natural join TYPEQUESTION where idQ='"+id+"' ORDER BY numQ");
        while(rs2.next()){
            System.out.println(rs2.getInt("numQ"));
            Question question = new Question(rs2.getString("texteQ"),rs2.getString("idT"),rs2.getInt("maxVal"),Integer.parseInt(id),rs2.getInt("numQ"));
            ResultSet reponse = st.executeQuery("SELECT valeur, sexe, intituleCat, valDebut, valFin, COUNT(idC) as nb FROM REPONDRE natural join CARACTERISTIQUE natural join TRANCHE natural join CATEGORIE where numQ='"+rs2.getString("numQ")+"' AND idQ='"+id+"' group by idQ, numQ, sexe, intituleCat, valDebut, valFin, valeur");
            if(rs2.getString("intituleT").equals("Classement")){
                while(reponse.next()){
                    String[] listSousReponse = reponse.getString("valeur").split("; ");
                    int numR = 1;
                    for(String sousReponse: listSousReponse){
                        question.ajoutReponduMultiple(new Reponse("Choix "+numR+" : "+sousReponse, reponse.getInt("nb"),reponse.getString("sexe"),reponse.getString("valDebut")+"-"+reponse.getString("valFin"),reponse.getString("intituleCat")));
                        numR++;
                    }
                }
            }else if(rs2.getString("intituleT").equals("Choix multiple")){
                while(reponse.next()){
                    String[] listSousReponse = reponse.getString("valeur").split("; ");
                    for(String sousReponse: listSousReponse){
                        question.ajoutReponduMultiple(new Reponse(sousReponse, reponse.getInt("nb"),reponse.getString("sexe"),reponse.getString("valDebut")+"-"+reponse.getString("valFin"),reponse.getString("intituleCat")));
                    }
                }
            }else{
                while(reponse.next()){
                    question.ajoutRepondu(new Reponse(reponse.getString("valeur"), reponse.getInt("nb"),reponse.getString("sexe"),reponse.getString("valDebut")+"-"+reponse.getString("valFin"),reponse.getString("intituleCat")));
                }
            }
            PreparedStatement psTraitement = laConnexion.prepareStatement("SELECT option, type from QUESTION natural join TRAITEMENTQUESTION natural join TRAITEMENT where idQ=? and numQ=?");
            psTraitement.setInt(1, Integer.parseInt(id));
            psTraitement.setInt(2, rs2.getInt("numQ"));
            ResultSet rsTraitement = psTraitement.executeQuery();
            while(rsTraitement.next()){
                question.addTraitement(new Traitement(rsTraitement.getString("type"), rsTraitement.getString("option")));
            }
            res.ajouterQuestion(question);
        }
        return res;
    }

    public void sauvegarderAnalyste(Questionnaire questionnaire) throws SQLException{
        PreparedStatement psDelete = laConnexion.prepareStatement("DELETE FROM TRAITEMENTQUESTION where idQ=? and numQ=?");
        PreparedStatement psGetIdTQ = laConnexion.prepareStatement("SELECT idTQ from TRAITEMENT where type = ? and option = ?");
        PreparedStatement psMaxId = laConnexion.prepareStatement("SELECT IFNULL(MAX(idTQ),0) as idTQmax from TRAITEMENT");
        PreparedStatement psInsertTraitement = laConnexion.prepareStatement("INSERT INTO TRAITEMENT(idTQ,type,option) VALUES (?,?,?)");
        PreparedStatement psAJoutTraitementQuestion = laConnexion.prepareStatement("INSERT INTO TRAITEMENTQUESTION (idQ,numQ,idTQ) VALUES (?,?,?)");
        for(Question question: questionnaire.getQuestion()){
            psDelete.setInt(1, questionnaire.getId());
            psDelete.setInt(2, question.getNumQ());
            psDelete.executeUpdate();
            for(Traitement traitement: question.getTraitement()){
                int idTQ = -1;
                psGetIdTQ.setString(1, traitement.getType());
                psGetIdTQ.setString(2, traitement.getOption());
                ResultSet rsGetIdTQ = psGetIdTQ.executeQuery();
                if(rsGetIdTQ.next()){
                    idTQ = rsGetIdTQ.getInt("idTQ");
                }else{
                    ResultSet rs = psMaxId.executeQuery();
                    rs.next();
                    idTQ = rs.getInt("idTQmax") + 1;
                    psInsertTraitement.setInt(1, idTQ);
                    psInsertTraitement.setString(2, traitement.getType());
                    psInsertTraitement.setString(3, traitement.getOption());
                    psInsertTraitement.executeUpdate();
                }
                psAJoutTraitementQuestion.setInt(1, questionnaire.getId());
                psAJoutTraitementQuestion.setInt(2, question.getNumQ());
                psAJoutTraitementQuestion.setInt(3, idTQ);
                psAJoutTraitementQuestion.executeUpdate();
            }
        }
    }
}
