import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurVisualisation implements EventHandler<ActionEvent> {

    /**
     * Attribut contenant la vue
     */
    private AppliAllo45 appli;

    /**
     * Constructeur faisant une liaison entre le controleur, la vue et la bd
     * @param app l'appli
     * @param questionnaire le questionnaire contenant les réponses à la question
     */
    public ControleurVisualisation(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Button buttonTarget = (Button)actionEvent.getTarget();
        // on teste les cas pour gérer les utilisations
        if(buttonTarget.getText().contains("Quitter")){
            this.appli.modeAnalyste();
        }
        // if(buttonTarget.getText().contains("Ajouter un graphique")){
        //     // ouvir pop up de choix de graphique
        //     this.appli.popUpChoixTypeGraphique();
        // }
        
        // if(buttonTarget.getText().contains("titre")){
        //     System.out.println("ahaahh");
        // }
    }
}
