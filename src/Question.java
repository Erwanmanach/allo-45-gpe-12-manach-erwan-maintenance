import java.util.List;
import java.util.ArrayList;

public class Question {
    private String intitule, reponse, type;
    private int etat, maxVal;
    private int idQ, numQ;
    private List<Reponse> repondu;
    private List<String> reponsePossible;
    private List<Traitement> traitements;
    public Question(String intitule, String type, int maxVal){
        this.intitule = intitule;
        this.maxVal = maxVal;
        this.type = type;
        this.repondu = new ArrayList<>();
        this.reponsePossible = new ArrayList<>();
        this.traitements = new ArrayList<>();
        this.etat = 1;
        this.idQ = 0;
        this.numQ = 0;
    }
    public Question(String intitule, String type, int maxVal,int idQ,int numQ){
        this.intitule = intitule;
        this.maxVal = maxVal;
        this.type = type;
        this.repondu = new ArrayList<>();
        this.reponsePossible = new ArrayList<>();
        this.traitements = new ArrayList<>();
        this.etat = 1;
        this.idQ = idQ;
        this.numQ = numQ;
    }
    public Question(String intitule, String type,int maxVal, String reponse,int etat){
        this.etat = etat;
        this.intitule = intitule;
        this.repondu = new ArrayList<>();
        this.reponsePossible = new ArrayList<>();
        this.traitements = new ArrayList<>();
        this.reponse = reponse;
        this.type = type;
        this.maxVal = maxVal;
        this.idQ = 0;
        this.numQ = 0;
    }
    public Question(String intitule, String type,int maxVal, String reponse,int etat,int idQ,int numQ){
        this.etat = etat;
        this.intitule = intitule;
        this.repondu = new ArrayList<>();
        this.reponsePossible = new ArrayList<>();
        this.traitements = new ArrayList<>();
        this.reponse = reponse;
        this.type = type;
        this.maxVal = maxVal;
        this.idQ = idQ;
        this.numQ = numQ;
    }
    /**
     * Add à répondu
     * @param rep la réponse à ajouter
     */
    public void ajoutRepondu(Reponse rep){this.repondu.add(rep);}
    /**
     * Add à répondu pour les questionnqire multiple
     * @param rep la réponse à ajouter
     */
    public void ajoutReponduMultiple(Reponse rep){
        boolean trouver = false;
        for(int i=0; i < this.repondu.size() && ! trouver; ++i){
            if(this.repondu.get(i).getIntitule().equals(rep.getIntitule()) && this.repondu.get(i).getSexe().equals(rep.getSexe())){
                trouver = true;
                this.repondu.get(i).ajouterNBreponse(rep.nbReponse());
            }
        }
        if(!trouver)repondu.add(rep);
    }
    /**
     * return the type of the question
     * @return the type of question
     */
    public String returnType(){return this.type;}
    /**
     * return the etat of question
     * @return etat of the question
     */
    public int getEtat(){return this.etat;}
    /**
     * retourne l'intitulé de la question
     * @return l'intitulé de la question
     */
    public String getIntituler(){return this.intitule;}
    /**
     * pour changer l'état de la question
     * @param etat à qu'elle état mettre la question
     */
    public void setEtat(int etat){this.etat = etat;}
    /**
     * for change the title of question
     * @param intitule the new title of question
     */
    public void setIntitule(String intitule){this.intitule = intitule;}
    /**
     * for add a Reponse of question
     * @param o the reponse a add
     */
    public void setReponse(String reponse){this.reponse = reponse;}
    /**
     * retourne les réponse remplit
     * @return les réponses qui ont été remplis
     */
    public String getReponse(){return this.reponse;}
    /**
     * retourne les réponse possibles
     * @return les réponses possibles à la question
     */
    public List<String> getReponsePossible(){return this.reponsePossible;}
    /**
     * retourne le nombres de réponse max ou la valeur max
     * @return la valeur max
     */
    public int getMaxVal(){return this.maxVal;}
    public void addReponsePossible(String reponsePossible){this.reponsePossible.add(reponsePossible);}
    public void setMaxVal(int maxVal){this.maxVal = maxVal;}
    public void validerQuestion(){this.etat = 3;}
    public String getType(){return this.type;}
    public List<Traitement> getTraitement(){return this.traitements;}
    public void addTraitement(Traitement traitement){this.traitements.add(traitement);}
    public void supprimerReponsepossible(String reponsepossible) throws ReponsenotFind{
        int indice = this.reponsePossible.indexOf(reponsepossible);
        if(indice == -1){
            throw new ReponsenotFind();
        }else{
            this.reponsePossible.remove(indice);
        }
    }
    public void resetReponsePossible(){this.reponsePossible = new ArrayList<>();}
    public List<Reponse> getRepondu(){return this.repondu;}
    public int getId(){return this.idQ;}
    public int getNumQ(){return this.numQ;}
}
