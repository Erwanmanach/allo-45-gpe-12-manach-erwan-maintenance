import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurChangerQuestion implements EventHandler<ActionEvent> {
    private AppliAllo45 appli;
    private Questionnaire questionnaire;
    public ControleurChangerQuestion(AppliAllo45 appli){
        this.appli = appli;
        this.questionnaire = appli.getQuestionnaire();
    }
    public void handle(ActionEvent actionEvent){
        Button boutton = (Button) actionEvent.getSource();
        if(this.appli.getRole().contains("C")){
            this.appli.sauvegardeQuestion();
        }else if(this.appli.getRole().contains("S")){
            this.appli.sauvegardeReponse();
        }
        if(boutton.getId().contains("suiv")){
            int num = this.questionnaire.numQ(this.appli.getQuestionActuelle());
            if(num < this.questionnaire.size()-1){
                this.appli.setQuestionActuelle(this.questionnaire.get(num+1));
            }else{
                if(this.appli.getRole().contains("S")){
                    this.appli.passerValidation();
                    System.out.println(5);
                }
            }
        }
        else if(boutton.getId().contains("prec")){
            int num = this.questionnaire.numQ(this.appli.getQuestionActuelle());
            if(this.appli.getRole().contains("S") && num == this.questionnaire.size()-1 && this.appli.getValidation()){
                this.appli.sortirValidation();
                this.appli.setQuestionActuelle(this.questionnaire.get(num));
            }else if(num > 0){
                this.appli.setQuestionActuelle(this.questionnaire.get(num-1));
                System.out.println(num);
            }
        }else{
            int id = Integer.parseInt(boutton.getId());
            this.appli.setQuestionActuelle(this.questionnaire.get(id));
        }
        if(this.appli.getRole().contains("C")){
            this.appli.miseAJourQuestionConcepteur();
        }
        else{
            this.appli.miseAJourQuestion();
        }
    }
}
