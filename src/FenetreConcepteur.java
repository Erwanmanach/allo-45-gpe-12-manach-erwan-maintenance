import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class FenetreConcepteur extends BorderPane {

    private int numeroReponse;
    private AppliAllo45 appli;

    public FenetreConcepteur(AppliAllo45 appli){ 
        super();

        this.appli = appli;
        this.setTop(this.banniere());
        this.setCenter(this.center());
        this.setRight(this.appli.getMenueDeroulant());
        this.setBottom(this.bottom());
        this.setStyle("-fx-padding: 10;-fx-background-color:#e7f2f3;");
    }

    // @Override
    // public void init() {
    //     this.entreprise = new Label("Atos");
    //     this.populationInterogee = new Label("jeunes");
    //     ImageView deco = new ImageView(new Image("deconnexion.png"));
    //     deco.setFitWidth(35);
    //     deco.setFitHeight(35);
    //     this.deconnexion = new Button("deconnexion", deco);
    //     this.deconnexion.setStyle("-fx-border-style: solid; ");

    //     this.numeroQuestion = 1; 
    //     this.numeroReponse = 1; 
    //     this.question = new TextField();
    //     this.question.setPromptText("intitulé de la question");
    // }

    private BorderPane center(){ // réponse unique (text area)
        BorderPane centre = new BorderPane();
        VBox topConcepteur = new VBox();       
        topConcepteur.setAlignment(Pos.CENTER);
        HBox laQuestion = new HBox();
        laQuestion.getChildren().addAll(this.appli.getnumQuestion());
        laQuestion.setPadding(new Insets(5));
        this.appli.getconcepteurQuestionActuel().setStyle("-fx-padding : 3; -fx-border-style: solid; -fx-border-radius : 10; -fx-background-radius :10 ");
        topConcepteur.getChildren().addAll(laQuestion,this.appli.getconcepteurQuestionActuel());

        ImageView imagePlus = new ImageView(new Image("plus.png"));
        imagePlus.setFitWidth(35);
        imagePlus.setFitHeight(35);
        Button ajoutReponse = new Button("Créer réponse", imagePlus);
        // ajoutReponse.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        // ajoutReponse.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        // ajoutReponse.setTextFill(Color.web("#137b8b"));
        ajoutReponse.setPrefSize(170, 100);
        ajoutReponse.setStyle("-fx-padding: 10; -fx-border-style: solid; -fx-border-radius: 10; -fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-background-radius :10");
        
        TilePane reponses = new TilePane();
        TextArea zoneRep = new TextArea();
        zoneRep.setPromptText("zone de réponse");
        zoneRep.setStyle("-fx-border-style: solid;");
        
        reponses.getChildren().addAll(this.appli.getReponses());
        centre.setTop(topConcepteur);
        centre.setCenter(reponses);
        centre.setStyle("-fx-padding: 10; -fx-border-style: solid; -fx-border-radius: 10;");
        reponses.setHgap(10);
        reponses.setPadding(new Insets(20));
        topConcepteur.setPadding(new Insets(20));

        centre.setStyle("-fx-padding : 1; -fx-border-style: solid; -fx-border-width : 5; -fx-border-color : #137b8b;  -fx-border-radius : 20; -fx-background-color: #f9f7f0;-fx-background-radius: 20;");
        return centre;
    }

    public VBox reponse(){
        this.numeroReponse++;
        TextArea zoneRep = new TextArea();
        zoneRep.setPromptText("zone de réponse");
        zoneRep.setStyle("-fx-border-style: solid;");
        VBox rep = new VBox(new Label("Reponse n°" + (this.numeroReponse)), zoneRep);
        rep.setPrefSize(170, 100);
        rep.setStyle("-fx-padding: 10; -fx-border-style: solid; -fx-border-radius: 10;");
        return rep;
    }


    //top left bottom right

    private BorderPane banniere(){
        BorderPane banniere = new BorderPane();
        banniere.setPadding(new Insets(0, 0, 20, 0));
        GridPane info = new GridPane();
        ImageView deco = new ImageView(new Image("deconnexion.png"));
        deco.setFitWidth(35);
        deco.setFitHeight(35);
        info.add(new Label("Entreprise : "),0,0);
        info.add(this.appli.getentreprise(),1,0);
        info.add(new Label("Population interogée : "),0,1);
        info.add(this.appli.getpanel(),1,1);
        info.setStyle("-fx-padding : 3; -fx-border-width : 4; -fx-border-color : #137b8b; -fx-border-style: solid; -fx-border-radius : 10; -fx-background-color: #f9f7f0; -fx-background-radius: 10;");
        banniere.setLeft(info);
        banniere.setRight(this.appli.getButtonDeconnexion());
        banniere.setStyle("-fx-margin: 100px;");
        return banniere;
    }

    private HBox bottom(){
        HBox bas = new HBox();
        HBox retour = new HBox();
        HBox boutonsQuestion = new HBox(40);
        bas.setPadding(new Insets(20, 0, 0, 0));
        ImageView Questionnaire = new ImageView(new Image("validerQuestionnaire.png"));
        Questionnaire.setFitWidth(35);
        Questionnaire.setFitHeight(35);
        Button validerQuestionnaire = new Button("Valider \nQuestionnaire", Questionnaire);
        validerQuestionnaire.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        validerQuestionnaire.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        validerQuestionnaire.setTextFill(Color.web("#137b8b"));
        validerQuestionnaire.setId("valider questionnaire");
        validerQuestionnaire.setOnAction(new ControleurConcepteur(appli));
        

        ImageView annuler = new ImageView(new Image("annuler.png"));
        annuler.setFitWidth(35);
        annuler.setFitHeight(35);
        Button annulerQuestion = new Button("Supprimer \nQuestion", annuler);
        annulerQuestion.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        annulerQuestion.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        annulerQuestion.setTextFill(Color.web("#137b8b"));
        annulerQuestion.setOnAction(new ControleurConcepteur(appli));
        annulerQuestion.setId("supprimer la question");
        
        ImageView newQuest = new ImageView(new Image("plus.png"));
        newQuest.setFitWidth(35);
        newQuest.setFitHeight(35);
        Button newQuestion = new Button("Nouvelle \nQuestion", newQuest);
        newQuestion.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        newQuestion.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        newQuestion.setTextFill(Color.web("#137b8b"));
        newQuestion.setId("nouvelle question");
        newQuestion.setOnAction(new ControleurConcepteur(appli));
        
        ImageView validerQuest = new ImageView(new Image("check.png"));
        validerQuest.setFitWidth(35);
        validerQuest.setFitHeight(35);
        Button validerQuestion = new Button("Valider \nQuestion", validerQuest);
        validerQuestion.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        validerQuestion.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        validerQuestion.setTextFill(Color.web("#137b8b"));
        validerQuestion.setId("valider la question");
        validerQuestion.setOnAction(new ControleurConcepteur(appli));
        boutonsQuestion.getChildren().addAll(validerQuestionnaire, annulerQuestion, newQuestion, validerQuestion);
        Image home = new Image("home.png");
        ImageView maison = new ImageView(home);
        maison.setFitHeight(35);
        maison.setFitWidth(35);
        Button boutonRetour = new Button("Pour retourner à la \nséléction des sondages",maison);
        boutonRetour.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        boutonRetour.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        boutonRetour.setTextFill(Color.web("#137b8b"));
        retour.getChildren().add(boutonRetour);
        retour.setPadding(new Insets(20,0,40,0));
        boutonRetour.setOnAction(new ControleurRetour(this.appli));
        HBox.setHgrow(retour, Priority.ALWAYS);
        boutonsQuestion.setAlignment(Pos.CENTER);
        retour.setAlignment(Pos.CENTER_RIGHT);
        bas.getChildren().addAll(boutonsQuestion, retour);
        return bas;
    }

    public BorderPane QuestionNote(){
        BorderPane fenetre = new BorderPane();
        // creation de l'ihm
        return fenetre;
    }

    public void modeQuestionNote(){
        this.setCenter(this.QuestionNote());
    }


}

   
    

   

  
