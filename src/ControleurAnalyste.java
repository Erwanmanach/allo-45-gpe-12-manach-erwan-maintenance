import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurAnalyste implements EventHandler<ActionEvent> {

    /**
     * Attribut contenant la vue
     */
    private AppliAllo45 appli;

    /**
     * Constructeur faisant une liaison entre le controleur, la vue et la bd
     * @param app l'appli
     */
    public ControleurAnalyste(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Button buttonTarget = (Button)actionEvent.getTarget();
        // on teste les cas pour gérer les utilisations
        if(buttonTarget.getText().contains("Graphique")){
            int i=4;
            String mot="";
            while(i<buttonTarget.getId().length()){
                mot+=buttonTarget.getId().charAt(i);
                i++;
            }
            Traitement cléTraitement = this.appli.getQuestionActuelle().getTraitement().get(Integer.parseInt(mot));
            this.appli.modeVisualisation(cléTraitement);
        }
        if(buttonTarget.getText().contains("Ajouter un graphique")){
            // ouvir pop up de choix de graphique
            String type = this.appli.popUpChoixTypeGraphique();
            if(type.equals("")){
            }else{
                String filtre = this.appli.popUpChoixFiltre();
                if(filtre.equals("")){
                }else{
                    this.appli.getQuestionActuelle().getTraitement().add(new Traitement(type, filtre));
                    this.appli.miseAJourTilePaneAnalyste();
                }
            }
        }if(buttonTarget.getText().contains("texte")){
            this.appli.modeVisualisation(new Traitement("tableau", "Aucun"));

        }
    }
}
