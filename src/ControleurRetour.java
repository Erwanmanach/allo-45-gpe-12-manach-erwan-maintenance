import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurRetour implements EventHandler<ActionEvent> {
    private AppliAllo45 appli;

    public ControleurRetour(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        if(this.appli.getRole().contains("C")){
            if(this.appli.getQuestionnaire()!=null){
                try{
                    this.appli.sauvegardeQuestion();
                    this.appli.getBDConcepteur().sauvegarde(this.appli.getQuestionnaire(), this.appli.getUtilisateur());
                }catch(SQLException sqle){
                    System.out.println(sqle);
                }
            }
        }else if(this.appli.getRole().equals("Analyste")){
            try{
                this.appli.sauvegardeTraitement();
            }catch(SQLException sqle){
                System.out.println(sqle);
            }
        }
        this.appli.modeSelection();
    }
}
