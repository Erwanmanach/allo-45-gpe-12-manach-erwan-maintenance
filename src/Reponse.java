public class Reponse {
    private String intitule;
    private int nbReponse;
    private String sexe;
    private String age;
    private String categorie;
    public Reponse(String intitule, int nbReponse,String sexe, String age, String categorie){
        this.intitule = intitule;
        this.nbReponse = nbReponse;
        this.sexe = sexe;
        this.age = age;
        this.categorie = categorie;
    }
    public String getIntitule(){return this.intitule;}
    public String getSexe(){return this.sexe;}
    public String getAge(){return this.age;}
    public String getCategorie(){return this.categorie;}
    public int nbReponse(){return this.nbReponse;}
    public void ajouterNBreponse(int nombreReponse){this.nbReponse += nombreReponse;}
}
