import java.sql.SQLException;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

public class ControleurConnexion implements EventHandler<ActionEvent> {
    private AppliAllo45 appli;
    private ConnexionBD connexionBD;

    public ControleurConnexion(AppliAllo45 appli, ConnexionBD connexionBD){
        this.appli = appli;
        this.connexionBD = connexionBD;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        Button b = (Button)actionEvent.getSource();
        if (b.getText().contains("Connexion")){
            String login = this.appli.getTextLogin();
            String mdp = this.appli.getTextMotDePasse();
            if (appli.verifieIdentifiant()){
                this.appli.afficherMessageMauvaisIdentifiant();
            }
            else{
                try{
                    Utilisateur user = this.connexionBD.connexion(login, mdp);
                    if (user == null){
                        this.appli.afficherMessageMauvaisIdentifiant();// Mettre une popup ou un texte en rouge
                    }
                    else{
                        this.appli.setUtilisateur(user);
                        this.appli.modeSelection();
                    }
                }
                catch (SQLException e){
                    this.appli.afficherMessageErreurConnexion();
                }
            }
        }
        else if (b.getText().contains("Deconnexion")){

            Optional<ButtonType> reponse = this.appli.popUpDeconnexion().showAndWait(); // on lance la fenêtre popup et on attends la réponse
            // si la réponse est oui
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                if(this.appli.getQuestionnaire()!=null){
                    if(this.appli.getRole().contains("C")){
                        try{
                            this.appli.sauvegardeQuestion();
                            this.appli.getBDConcepteur().sauvegarde(this.appli.getQuestionnaire(), this.appli.getUtilisateur());
                        }catch(SQLException sqle){
                            System.out.println(sqle);
                        }
                    }else if(this.appli.getRole().equals("Analyste")){
                        try{
                            this.appli.sauvegardeTraitement();
                        }catch(SQLException sqle){
                            System.out.println(sqle);
                        }
                    }
                }
                appli.setUtilisateur(null);
                appli.modeConnexion();
            }
            else if(reponse.isPresent() && reponse.get().equals(ButtonType.NO)){
                appli.setUtilisateur(null);
                appli.modeConnexion();
            }else{}
        }
    }
}
