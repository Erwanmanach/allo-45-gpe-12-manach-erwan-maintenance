import java.sql.*;
import java.util.List;
import java.util.ArrayList;

public class ConnexionBD {
	ConnexionMySQL laConnexion;
	Statement st;
    public ConnexionBD(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }
    public Utilisateur connexion(String login, String password) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement("SELECT nomU, prenomU, nomR, idU, numC from UTILISATEUR natural join ROLEUTIL where login=? AND motDePasse=?");
        ps.setString(1, login);
        ps.setString(2, password);
		ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Utilisateur user = new Utilisateur(rs.getString("nomR"), rs.getString("prenomU"), rs.getString("nomU"), rs.getInt("idU"));
            if(rs.getString("nomR").contains("E")){
                user.setNumC(rs.getInt("numC"));
            }
            return user;
        }
        return null;
    }

    public List<List<String>> listeSondage(String nomR) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement("SELECT Titre, nomPan, idQ from QUESTIONNAIRE natural join PANEL where Etat=?");
        ps.setString(1, ""+nomR.charAt(0));
        List<List<String>> res = new ArrayList<>();
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            List<String> ligne = new ArrayList<String>();
            ligne.add(rs.getString("Titre"));
            ligne.add(rs.getString("nomPan"));
            ligne.add(rs.getString("idQ"));
            res.add(ligne);
        }
        return res;
    }

    public List<List<String>> listeSondageEntreprise(int numC) throws SQLException{
        PreparedStatement ps = laConnexion.prepareStatement("SELECT Titre, nomPan, idQ from QUESTIONNAIRE natural join PANEL where Etat='A' and numC=?");
        ps.setInt(1, numC);
        List<List<String>> res = new ArrayList<>();
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            List<String> ligne = new ArrayList<String>();
            ligne.add(rs.getString("Titre"));
            ligne.add(rs.getString("nomPan"));
            ligne.add(rs.getString("idQ"));
            res.add(ligne);
        }
        return res;
    }
}
