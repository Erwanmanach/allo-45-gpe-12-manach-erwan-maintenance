import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;

import java.util.HashMap;

import javafx.geometry.Insets;
import javafx.geometry.Side;

public class FenetreVisualisation extends BorderPane{
    private Label entreprise;
    private AppliAllo45 appli;
    private Label populationInterogee;

    public FenetreVisualisation(AppliAllo45 appli,Traitement cléTraitement){
        super();
        this.appli = appli;
        this.setStyle("-fx-background-color:#e7f2f3;");
        this.setPadding(new Insets(20));
        this.entreprise = new Label(this.appli.getQuestionnaire().getEntreprise());
        this.populationInterogee = new Label(this.appli.getQuestionnaire().getPanel()); 
        this.setTop(this.banniere());
        this.setCenter(this.visualisation(cléTraitement));
        ImageView retour = new ImageView("retour.png");
        retour.setFitHeight(35);
        retour.setFitWidth(35);
        Button quitterVisualisation = new Button("Quitter la visualisation",retour);
        quitterVisualisation.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        quitterVisualisation.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        quitterVisualisation.setTextFill(Color.web("#137b8b"));
        quitterVisualisation.setId("quitterVisu");
        quitterVisualisation.setOnAction(new ControleurVisualisation(this.appli));
        this.setBottom(quitterVisualisation);
    }
       
    //top de la page de de conception/analyste
    private BorderPane banniere(){
        BorderPane banniere = new BorderPane();
        GridPane info = new GridPane();
        info.add(new Label("Entreprise : "),0,0);
        info.add(this.entreprise,1,0);
        info.add(new Label("Population interogée : "),0,1);
        info.add(this.populationInterogee,1,1);
        info.setStyle("-fx-padding : 3; -fx-border-width : 4; -fx-border-color : #137b8b; -fx-border-style: solid; -fx-border-radius : 10; -fx-background-color: #f9f7f0; -fx-background-radius: 10;");
        banniere.setLeft(info);
        banniere.setRight(this.appli.getButtonDeconnexion());
        banniere.setPadding(new Insets(0,0,20,0));
        return banniere;
    }

    // centre de la page d'analyse comprenant les outils
    private BorderPane visualisation(Traitement cléTraitement){
        BorderPane visualisation = new BorderPane();
        if(cléTraitement.getType().contains("B")){
            visualisation.setCenter(estbarchartsimple(cléTraitement));
        }
        else if(cléTraitement.getType().equals("tableau")){
            visualisation.setCenter(tableauBrute(cléTraitement));
        }
        else{
            visualisation.setCenter(estpieChart(cléTraitement));
        }return visualisation;
    }
    private PieChart estpieChart(Traitement cléTraitement){
        PieChart chart = new PieChart();
        chart.setTitle(this.appli.getQuestionActuelle().getIntituler());
        HashMap<String,Double> repondu = transformSimple(cléTraitement);
        for(String rep: repondu.keySet()){
            chart.getData().add(new PieChart.Data(rep,repondu.get(rep)));
        }
        chart.setLegendSide(Side.LEFT);
        return chart;
    }
    private BarChart<String, Number> estbarchartsimple(Traitement cléTraitement){
        NumberAxis xAxis = new NumberAxis();
        CategoryAxis yAxis = new CategoryAxis();
        BarChart<String, Number> barchart = new BarChart<>(yAxis, xAxis);
        xAxis.setLabel("Note");
        xAxis.setTickLabelRotation(90);
        yAxis.setLabel("reponse donnés");
        XYChart.Series<String, Number> premier = new XYChart.Series<>();
        premier.setName("note données");
        HashMap<String,Double> repondu = transformSimple(cléTraitement);
        for(String rep: repondu.keySet()){
            premier.getData().add(new XYChart.Data<>(rep,repondu.get(rep)));
        }
        barchart.getData().add(premier);
        barchart.setLegendSide(Side.LEFT);
        return barchart;
    }
    private Text tableauBrute(Traitement cléTraitement){
        HashMap<String,Double> repondu = transformSimple(cléTraitement);
        String sauve = "note  |  valeur\n";
        for(String rep: repondu.keySet()){
            sauve+=rep+"  | "+repondu.get(rep)+"\n";
        }
        return new Text(sauve);
    }
    public HashMap<String,Double> transformSimple(Traitement transform){
        HashMap<String,Double> res = new HashMap<>();
        for(Reponse rep: this.appli.getQuestionActuelle().getRepondu()){
            if(transform.getOption().equals("Aucun")){
                if(!(res.keySet().contains(rep.getIntitule()))){
                    res.put(rep.getIntitule(), 0.0);
                }
                res.replace(rep.getIntitule(),(rep.nbReponse()+res.get(rep.getIntitule())));
            }else if(transform.getOption().equals("Sexe")){
                if(!(res.keySet().contains(rep.getSexe()+" "+rep.getIntitule()))){
                    res.put(rep.getSexe()+" "+rep.getIntitule(), 0.0);
                }
                res.replace(rep.getSexe()+" "+rep.getIntitule(),(rep.nbReponse()+res.get(rep.getSexe()+" "+rep.getIntitule())));
            }else if(transform.getOption().equals("Catégorie")){
                if(!(res.keySet().contains(rep.getCategorie()+" "+rep.getIntitule()))){
                    res.put(rep.getCategorie()+" "+rep.getIntitule(), 0.0);
                }
                res.replace(rep.getCategorie()+" "+rep.getIntitule(),(rep.nbReponse()+res.get(rep.getCategorie()+" "+rep.getIntitule())));
            }else if(transform.getOption().equals("Age")){
                if(!(res.keySet().contains(rep.getAge()+" "+rep.getIntitule()))){
                    res.put(rep.getAge()+" "+rep.getIntitule(), 0.0);
                }
                res.replace(rep.getAge()+" "+rep.getIntitule(),(rep.nbReponse()+res.get(rep.getAge()+" "+rep.getIntitule())));
            }
        }
        return res;
    }
}
