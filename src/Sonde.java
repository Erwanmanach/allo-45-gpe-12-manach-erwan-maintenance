public class Sonde {
    private String nom, prenom, numTel, idC;
    private int numSonde;
    public Sonde(String nom, String prenom, String numTel,String idC,int numSonde){
        this.nom = nom;
        this.prenom = prenom;
        this.numTel = numTel;
        this.idC = idC;
        this.numSonde = numSonde;
    }
    public String getRole(){return this.numTel;}
    public String getNom(){return this.nom;}
    public String getPrenom(){return this.prenom;}
    public String getIdc(){return this.idC;}
    public int getNumSonde(){return this.numSonde;}
    public void setRole(String numTel){ this.numTel = numTel;}
    public void setNom(String nom){this.nom = nom;}
    public void setprenom(String prenom){this.prenom = prenom;}
    public void setIdc(String idC){this.idC = idC;}
    public void setNumSonde(int numSonde){this.numSonde = numSonde;}
}
