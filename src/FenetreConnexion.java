import java.awt.Dimension;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class FenetreConnexion extends BorderPane {
    private PasswordField motDePasse;
    private TextField login;
    private Text erreur;
    private Text messageErreurIdentifiants;

   
    public FenetreConnexion(AppliAllo45 appli){
        super();
        this.erreur = new Text("");
        this.setPadding(new Insets(10));

        this.motDePasse = appli.getMotDePasse();
        this.login = appli.getLogin(); 
        this.messageErreurIdentifiants = new Text();
        
        Label titre=new Label("Entrez votre identifiant et votre mot de passe. ");
        titre.setStyle("-fx-font-weight:bold;-fx-font-size:1.2em;-fx-padding:4px;");
        titre.setTextFill(Color.web("#fffbf0")); 
        Label ln=new Label("Identifiant :");
        ln.setTextFill(Color.web("#fffbf0")); 
        Label lp=new Label("Mot de passe :");
        lp.setTextFill(Color.web("#fffbf0")); 
        
        ImageView iconDeconnexion = new ImageView(new Image("connexion.png"));
        iconDeconnexion.setFitHeight(40);
        iconDeconnexion.setFitWidth(40);
        
        Button boutonConnexion= new Button("Connexion ",iconDeconnexion);
        boutonConnexion.setContentDisplay(ContentDisplay.RIGHT);
        this.setOnKeyPressed(new ControleurEntrer(appli, appli.getConnexionBD()));
        boutonConnexion.setOnAction(new ControleurConnexion(appli,appli.getConnexionBD()));
        boutonConnexion.setStyle("-fx-background-color: #fffbf0;");
        boutonConnexion.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 18.0));
        boutonConnexion.setTextFill(Color.web("#137b8b"));

        
        // on met le bouton de connexion dans une hBox pour pouvoir le centrer facilement
        HBox connexion = new HBox();  
        connexion.getChildren().add(boutonConnexion);
        connexion.setAlignment(Pos.CENTER_RIGHT);
        
        // on créée la zone ou on va mettres les champs
        GridPane menu = new GridPane();
        menu.add(titre,1,0,2,1);
        menu.add(ln,1,1);
        menu.add(lp,1,2);
        menu.add(this.login,2,1);
        menu.add(this.motDePasse,2,2);
        menu.add(connexion,2,3);
        menu.setAlignment(Pos.CENTER);
        
        menu.setHgap(15);
        menu.setVgap(15);

        // ajout de la bordure autour de la VBox
        menu.setStyle("-fx-padding: 10;" + 
                      "-fx-background-insets: 5;" + 
                      "-fx-background-radius: 20;" + 
                      "-fx-background-color: #137b8b;"+
                      "-fx-border-insets: 5;" + 
                      "-fx-border-width: 10px;" +
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: #b4dbdc;");

        menu.setMaxWidth(500);
        menu.setPrefHeight(250);
        menu.setAlignment(Pos.CENTER);

        //Creation du logo
        ImageView logo = new ImageView(new Image("logo.png"));
        logo.setPreserveRatio(true);
        logo.setFitWidth(300);

        
        

        // on créée l'écran entier
        VBox ecran = new VBox();
        ecran.getChildren().addAll(logo,messageErreurIdentifiants, menu,erreur);
        ecran.setAlignment(Pos.TOP_CENTER);
        ecran.setPadding(new Insets(80,0,0,0));

        this.setCenter(ecran);


        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int)dimension.getHeight();
        int width  = (int)dimension.getWidth();
        this.setBackground(new Background(new BackgroundImage(new Image("fondConnexion.png", width, height, false, true), null,null,null,null)));

    }

    public void erreurConnexion() {
        this.erreur.setText("Erreur de connexion");
    }

    public void erreurIdentifiant() {
        this.erreur.setText("Erreur dans les identifiants");
    }



    


}