public class Utilisateur {
    private String role, nomUtil, prenomUtil;
    private int id, numC;
    public Utilisateur(String role, String nomUtil, String prenomUtil, int id){
        this.role = role;
        this.nomUtil = nomUtil;
        this.prenomUtil = prenomUtil;
        this.id = id;
        this.numC = 0;
    }
    public String getRole(){return this.role;}
    public String getNomUtil(){return this.nomUtil;}
    public String getPrenomUtil(){return this.prenomUtil;}
    public void setRole(String role){ this.role = role;}
    public void setNomUtil(String nomUtil){this.nomUtil = nomUtil;}
    public void setprenomUtil(String prenomUtil){this.prenomUtil = prenomUtil;}
    public int getId(){return this.id;}
    public int getNumC(){return this.numC;}
    public void setNumC(int numC){this.numC = numC;}
}
