import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class AppliAllo45 extends Application{

    private Scene scene;

    private PasswordField motDePasse;
    
    private TextField login;

    private BorderPane root;

    private Utilisateur user;

    private Question questionActuelle;

    private Questionnaire questionnaire;

    private ConnexionBD connexionBD;

    private BDQuestionnaire bdQuestionnaire;

    private BDSondeur bdSondeur;

    private ConnexionMySQL connexionMySQL;

    private Text entreprise;

    private Text panel;

    private Text analysteQuestionActuel;

    private TextField concepteurQuestionActuel;

    private Text analysteIntituleQuestion;

    private Button deconnexion;

    private Button validerReponse;

    private TitledPane menuDeroulant;

    private TilePane reponses;

    private BDConcepteur bdConcepteur;
    
    private TilePane tilePane;

    private Text numQuestion;

    private ToggleGroup questionunique;

    private TextField zonenote;

    private TextArea zoneLibre;

    private TextField RS;

    private TextField AD1;

    private TextField AD2;

    private TextField CP;

    private TextField V;

    private TextField Tel;

    private TextField Email;

    private boolean validation;

    private TextField zoneNoteReponse;

    private TextArea zoneTextReponse;

    private Sonde sonde;

    private List<String> option;
    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.AD1 = new TextField();
        this.AD2 = new TextField();
        this.CP = new TextField();
        this.Email = new TextField();
        this.RS = new TextField();
        this.Tel = new TextField();
        this.V = new TextField();
        this.validation = false;
        this.tilePane = new TilePane();
        this.root = new BorderPane();
        this.motDePasse = new PasswordField();
        this.login = new TextField();
        this.concepteurQuestionActuel = new TextField();
        this.zonenote = new TextField();
        this.questionunique = new ToggleGroup();
        this.zoneLibre = new TextArea();
        this.panel = new Text();
        this.entreprise = new Text();
        this.numQuestion = new Text();
        this.analysteQuestionActuel = new Text();
        this.analysteIntituleQuestion = new Text();
        this.initDeconnexion();
        this.initValiderQuestionnaire();
        this.menuDeroulant = new TitledPane();
        this.menuDeroulant.setText("Questions");
        this.questionnaire = null;
        this.reponses = this.creer1ereReponse();
        this.connexionMySQL =  null;
        try{
            connexionMySQL = new ConnexionMySQL();
            connexionMySQL.connecter();
        }
        catch(Exception e){}

        this.connexionBD = null;
        try{
            connexionBD = new ConnexionBD(connexionMySQL);
        }
        catch(Exception e){}
        
        this.bdQuestionnaire = null;
        try{
            this.bdQuestionnaire = new BDQuestionnaire(connexionMySQL);
        }
        catch(Exception e){}
        
        this.bdSondeur = null;
        try{
            this.bdSondeur = new BDSondeur(connexionMySQL, this);
        }
        catch(Exception e){}

        this.bdConcepteur = new BDConcepteur(connexionMySQL);
        

    }

    public void initValiderQuestionnaire(){
        this.validerReponse = new Button("Validation du sondage");
        validerReponse.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        validerReponse.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 24));
        validerReponse.setTextFill(Color.web("#137b8b"));
        validerReponse.setContentDisplay(ContentDisplay.CENTER);
        validerReponse.setOnAction(new ControleurReponse(this));
        validerReponse.setPrefWidth(500);}

    public void initDeconnexion(){
        ImageView deco = new ImageView("deconnexion.png");
        deco.setFitWidth(35);
        deco.setFitHeight(35);
        this.deconnexion = new Button("Deconnexion",deco);  
        this.deconnexion.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10");
        this.deconnexion.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 18.0));
        this.deconnexion.setTextFill(Color.web("#137b8b"));
        this.deconnexion.setContentDisplay(ContentDisplay.RIGHT);
        this.deconnexion.setOnAction(new ControleurConnexion(this, this.connexionBD)); 
    }

    public void menuChoixQuestion(){
        GridPane lesQuestions = new GridPane();
        ScrollPane scroll = new ScrollPane(lesQuestions);
        this.menuDeroulant.setContent(scroll);
        menuDeroulant.setPadding(new Insets(0,0,0,20));
        menuDeroulant.setMaxWidth(Double.MAX_VALUE);
        VBox.setVgrow(menuDeroulant, Priority.ALWAYS);
        int ligne = 0;
        for(int i=0;i<this.questionnaire.size();++i){
            Button boutonligne = this.question(i);
            if(this.getRole().contains("C")){
                if(this.questionnaire.get(i).getEtat() == 1){
                    boutonligne.setStyle("-fx-background-color: red; -fx-padding: 0 10 0 10; -fx-border-style: solid;");
                }
                else if(this.questionnaire.get(i).getEtat() == 2){
                    boutonligne.setStyle("-fx-background-color: orange; -fx-padding: 0 10 0 10; -fx-border-style: solid;");
                }
                else{
                    boutonligne.setStyle("-fx-background-color: green; -fx-padding: 0 10 0 10; -fx-border-style: solid;");
                }
            }
            lesQuestions.add(boutonligne,0,i);
            ++ligne;
        }if(this.user.getRole().equals("Concepteur")){
            Button newQuestion = new Button("+ ajouter question");
            newQuestion.setOnAction(new ControleurConcepteur(this));
            newQuestion.setId("nouvelle question");
            newQuestion.setStyle("-fx-padding: 0 10 0 10; -fx-border-style: solid;");
            newQuestion.setMaxWidth(Double.MAX_VALUE);
            lesQuestions.add(newQuestion,0,ligne+1);
        }
    }

    private Button question(int indice){
        HBox numero = new HBox(new Label(""+(indice+1)));
        numero.setStyle("-fx-padding: 1 10 1 10; -fx-border-style: hidden solid hidden hidden;");//quadrillage du numéro
        HBox titre = new HBox(new Label(this.adapte(this.questionnaire.get(indice).getIntituler())));
        titre.setStyle("-fx-padding: 1 10 1 10; -fx-border-style: hidden hidden hidden solid;");//quadrillage du titre
        HBox quest = new HBox();
        quest.getChildren().addAll(numero, titre);
        Button bouton = new Button("", quest);
        bouton.setStyle("-fx-padding: 0 10 0 10; -fx-border-style: solid;");
        bouton.setId(""+(indice));
        if(this.getRole().equals("C")){
            bouton.setOnAction(new ControleurConcepteur(this.questionnaire, this)); 
        }
        else{
           bouton.setOnAction(new ControleurAnalyste(this)); 
        }
        bouton.setMaxWidth(Double.MAX_VALUE);
        bouton.setOnAction(new ControleurChangerQuestion(this));
        return bouton;
    }

    private String adapte(String aCouper){
        String res = "";
        String mot = "";
        int indice = 0;
        int taille = 0;
        while(indice < aCouper.length()){
            if(taille == 30){
                taille = mot.length();
                res+="\n";
            }if(aCouper.charAt(indice) == ' '){
                res += mot;
                mot="";
            }mot+=aCouper.charAt(indice);
            ++indice;
            ++taille;
        }
        res+=mot;
        return res;
    }

    @Override
    public void start(Stage stage) throws Exception {
        modeConnexion();
        //modeConcepteur();
        //modeSelection();
        //modeAnalyste();
        

        /* creation de la fenêtre à la bonne taille pour chaque écran */

        Rectangle2D screenBounds = Screen.getPrimary().getBounds();
        //System.out.println("Height: " + screenBounds.getHeight() + " Width: " + screenBounds.getWidth());
        this.scene = new Scene(root, screenBounds.getWidth(), screenBounds.getHeight());
        stage.setScene(scene);
        stage.setTitle("Allo 45");
        stage.show();
        //popUpChoixTypeQuestion();
        // popUpChoixEntreprise();
        // popUpChoixPanel();
    }


    public String popUpChoixPanel(){

        List<String> choice;
        try {
            choice = bdConcepteur.getPanel();
            ChoiceDialog<String> dialog = new ChoiceDialog<>(choice.get(0),choice );
            dialog.setTitle("Boite de dialogue du choix de la question"); //changer les textes
            dialog.setHeaderText("Indiquer le panel voulu");
            dialog.setContentText("Choisissez le panel voulu :");

            ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK)).setText("valider");
            ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("annuler la creation de question");

            // pour obtenir le resultat
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()){
                return result.get();
            }

            else{
                return "";
            }
        } catch (SQLException e) {
            return "";

        }
        
    }

    public String popUpSupprimerQuestion() {
        String res = "";
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Supprimer la question");
        alert.setHeaderText("Etes-vous sûr de vouloir suprimer la question ?");
  
        
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK){
            res = "OK";
        }
        return res;
    }

    public String popUpValiderQuestionnaire() {
        String res = "";
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Valider le questionnaire");
        alert.setHeaderText("Etes-vous sûr de vouloir valider le questionnaire ?");
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK){
            res = "OK";
        }
        return res;
    }


    public String popUpChoixEntreprise(){

        List<String> choice;
        try {
            choice = bdConcepteur.getNomEntreprise();
            ChoiceDialog<String> dialog = new ChoiceDialog<>(choice.get(0),choice );
            dialog.setTitle("Boite de dialogue du choix de la question");//changer les textes
            dialog.setHeaderText("Choix de l'entreprise");
            dialog.setContentText("Choisissez l'entreprise :");

            ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK)).setText("valider");
            ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("annuler la creation de question");

            // pour obtenir le resultat
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()){
                return result.get();
            }

            else{
                return "";
            }
        } catch (SQLException e) {
            return "";

        }
        
    }

    public String popUpChoixTitreQuestionnaire(){
        TextInputDialog dialog = new TextInputDialog("");
        
        dialog.setTitle("Titre du Questionnaire");
        dialog.setHeaderText("Choisir le titre de votre Questionnaire");
        dialog.setContentText("Votre titre :");

        Optional<String> result = dialog.showAndWait();
        return result.get();
    }

    public BDConcepteur getBDConcepteur(){return this.bdConcepteur;}

    public TilePane getCentre(){return this.tilePane;}

    public Text getnumQuestion(){return this.numQuestion;}

    public Text getentreprise(){return this.entreprise;}

    public Text getpanel(){return this.panel;}

    public Text getanalysteQuestionActuel(){return this.analysteQuestionActuel;}

    public TextField getconcepteurQuestionActuel(){return this.concepteurQuestionActuel;}

    public Text getanalysteIntituleQuestion(){return this.analysteIntituleQuestion;}

    public Button getButtonDeconnexion(){return this.deconnexion;}

    public TitledPane getMenueDeroulant(){return this.menuDeroulant;}

    public TextField getAD1() {
        return AD1;
    }

    public TextField getAD2() {
        return AD2;
    }

    public TextField getCP() {
        return CP;
    }


    public TextField getEmail() {
        return Email;
    }


    public TextField getRS() {
        return RS;
    }

    public TextField getTel() {
        return Tel;
    }


    public TextField getV() {
        return V;
    }
    
    
    public TextField getLogin() {
        return login;
    }



    public BDQuestionnaire getBdQuestionnaire() {
        return bdQuestionnaire;
    }

    public BDSondeur getBDSondeur() {
        return bdSondeur;
    }

    public ConnexionBD getConnexionBD() {
        return connexionBD;
    }

    public ConnexionMySQL getConnexionMySQL() {
        return connexionMySQL;
    }

    public void setBdQuestionnaire(BDQuestionnaire bdQuestionnaire) {
        this.bdQuestionnaire = bdQuestionnaire;
    }

    public void setPanel(String panel) {
        this.panel.setText(panel);;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise.setText(entreprise);
    }

    public void setConnexionBD(ConnexionBD connexionBD) {
        this.connexionBD = connexionBD;
    }

    public void setConnexionMySQL(ConnexionMySQL connexionMySQL) {
        this.connexionMySQL = connexionMySQL;
    }


    public PasswordField getMotDePasse() {
        return motDePasse;
    }

    public String getTextLogin() {
        return login.getText();
    }

    public String getTextMotDePasse() {
        return motDePasse.getText();
    }

    public BorderPane getRoot() {
        return root;
    }

    public Scene getScene() {
        return scene;
    }

    public Utilisateur getUtilisateur(){
        return this.user;
    }

    public Question getQuestionActuelle(){
        return this.questionActuelle;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public String getRole(){
        return this.user.getRole();
    }

    public int getNumC(){
        return this.user.getNumC();
    }

    public TilePane getReponses(){
        return this.reponses;
    }

    public void setReponses(TilePane pane){
        this.reponses = pane;
    }

    

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }



    public void setUtilisateur(Utilisateur user) {
        this.user = user;
    }

    public void setQuestionActuelle(Question questionActuelle) {
        this.questionActuelle = questionActuelle;
    }

    public void setnumQuestion(int numQuestion){this.numQuestion.setText("Question n° " + numQuestion);}

    public static void main(String[] args) {
        launch(args);
    }


    /* les changements d'affichage de fenêtre : */

    public void modeConnexion(){
        this.motDePasse.setText("");
        this.login.setText("");
        this.root.setCenter(new FenetreConnexion(this));
    }

    public void modeSelection(){
        this.validation = false;
        this.root.setCenter(new FenetreSelection(this));
    }


    public void modeConcepteur(){
        this.root.setCenter(new FenetreConcepteur(this));
    }

    public void modeAnalyste(){
        this.root.setCenter(new FenetreAnalyste(this));
    }

    public void modeVisualisation(Traitement type){
            this.root.setCenter(new FenetreVisualisation(this, type));
    }

    public ImageView donneImage(String type){
        if(type.contains("P")){
            return new ImageView("pie.png");
        }
        else if(type.contains("L")){
            return new ImageView("line.png");
        }
        else if(type.contains("Z")){
            return new ImageView("spider.png");
        }
        else if(type.contains("B")){
            return new ImageView("bar.png");
        }
        else {
            return new ImageView("pie.png");
        }
    }


    public TilePane creer1ereReponse(){
        TilePane fenetre = new TilePane();
        ImageView imagePlus = new ImageView(new Image("plus.png"));
        imagePlus.setFitWidth(35);
        imagePlus.setFitHeight(35);
        Button ajoutQuestion = new Button("Créer réponse", imagePlus);
        ajoutQuestion.setOnAction(new ControleurConcepteur(this));
        ajoutQuestion.setPrefSize(170, 100);
        ajoutQuestion.setStyle("-fx-padding: 10; -fx-border-style: solid; -fx-border-radius: 10; -fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-background-radius :10");
        fenetre.getChildren().addAll(ajoutQuestion);
        TilePane.setAlignment(ajoutQuestion, Pos.CENTER);
        return fenetre;
    }

    public void modeCreeEntreprise(){
        this.root.setCenter(new FenetreCreationEntreprise(this));
    }



    public String popUpChoixTypeQuestion(){

        List<String> choices = new ArrayList<>();
        choices.add("Libre");
        choices.add("Classement");
        choices.add("Note");
        choices.add("Multiple");
        choices.add("Unique");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Libre", choices);
        dialog.setTitle("Boite de dialogue du choix de la question");
        dialog.setHeaderText("Choix du type de question");
        dialog.setContentText("Choisissez un type de question :");

        ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK)).setText("valider");
        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("annuler la creation de question");

        // pour obtenir le resultat
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            return result.get();
        }
        else{
            return "";
        }
    }

    public void afficherMessageMauvaisIdentifiant(){
        FenetreConnexion fenetre = (FenetreConnexion) this.root.getCenter();
        fenetre.erreurIdentifiant();
    }

    public void afficherMessageErreurConnexion(){
        FenetreConnexion fenetre = (FenetreConnexion) this.root.getCenter();
        fenetre.erreurConnexion();
    }

    public boolean verifieIdentifiant(){ return (getTextLogin().length()==0 || getTextMotDePasse().length()==0);}

    public String popUpCreationTitreQuestion(){
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Boite de dialogue du choix de la question");
        dialog.setHeaderText("Choix du titre de la question");
        dialog.setContentText("Veuillez entrer le titre de la question:");
    
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            return result.get();
        }
        else{
            return "";}
        }
    public String popUpChoixTypeGraphique(){

        List<String> choices = new ArrayList<>();
        choices.add("Line chart");
        choices.add("Pie chart");
        choices.add("Zone chart");
        choices.add("Radar chart");
        choices.add("Bar chart");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("Pie chart", choices);
        dialog.setTitle("Boite de dialogue du choix du graphique");
        dialog.setHeaderText("Choix du type de graphique");
        dialog.setContentText("Choisissez un type de graphique :");

        // ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK)).setText("valider");
        ((Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("annuler la creation du graphique");
        ((Button) dialog.getDialogPane().lookupButton(ButtonType.OK)).setText("Choisir un filtre");

        // pour obtenir le resultat
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() ){
            return result.get();
        }
        else{
            return "";
        }
    }

    public String popUpChoixFiltre(){

        List<String> choicesFilter = new ArrayList<>();
        choicesFilter.add("Aucun");
        choicesFilter.add("Sexe");
        choicesFilter.add("Age");
        choicesFilter.add("Catégorie");

        ChoiceDialog<String> dialogFilter = new ChoiceDialog<>("Aucun", choicesFilter);
        dialogFilter.setTitle("Boite de dialogue du choix des filtres");
        dialogFilter.setHeaderText("Choix du type de filtre");
        dialogFilter.setContentText("Choisissez un type de filtre :");

        ((Button) dialogFilter.getDialogPane().lookupButton(ButtonType.OK)).setText("valider");
        ((Button) dialogFilter.getDialogPane().lookupButton(ButtonType.CANCEL)).setText("annuler l'ajout du graphique");
        
        Optional<String> result = dialogFilter.showAndWait();
        if (result.isPresent() ){
            return result.get();
        }
        else{
            return "";
        }
    }


    public Alert popUpDeconnexion(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Voulez-vous validez vox choix? ",ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.setTitle("Validation");
        return alert;
    }

    // public String popUpEntreprise(){

    //     TextInputDialog dialog = new TextInputDialog("");
    //     dialog.setTitle("Choix de l'entreprise");
    //     dialog.setHeaderText("Choisissez votre enreprise");
    //     dialog.setContentText("Pour quelle entreprise est destinée ce sondage:");

    //     Optional<String> result = dialog.showAndWait();
    //     if (result.isPresent()){
    //         System.out.println("Your name: " + result.get());
    //         return result.get();
    //     }
    //     else{
    //         return "";
    //     }

    // }

    // public String popUpPanel(){

    //     TextInputDialog dialog = new TextInputDialog("");
    //     dialog.setTitle("Choix du panel");
    //     dialog.setHeaderText("Choisissez votre Panel");
    //     dialog.setContentText("Pour quel Panel est destiné ce sondage:");

    //     Optional<String> result = dialog.showAndWait();
    //     if (result.isPresent()){
    //         System.out.println("Your name: " + result.get());
    //         return result.get();
    //     }
    //     else{
    //         return "";
    //     }

    // }

    public void miseAJourChoixSondage(){
        this.entreprise.setText(this.questionnaire.getEntreprise());
        this.panel.setText(this.questionnaire.getPanel());
        this.menuChoixQuestion();
        if(this.questionActuelle!=null){
            this.miseAJourQuestion();
            this.setnumQuestion(this.questionnaire.indexOf(this.questionActuelle)+1);
        }
    }
    
    public void miseAJourQuestion(){
        this.analysteIntituleQuestion.setText(this.questionActuelle.getIntituler());
        this.setnumQuestion(this.questionnaire.indexOf(this.questionActuelle)+1);
        if(this.user.getRole().contains("A") || this.user.getRole().equals("Entreprise")){
            this.miseAJourTilePaneAnalyste();
        }else if(this.user.getRole().contains("C")){
            this.miseAJourTilePaneConcepteur();
        }else{
            this.miseAJourTilePaneSondeur();
        }
    }

    public void miseAJourTilePaneAnalyste(){
        int position = 0;
        tilePane.getChildren().clear();
        for(Traitement traitement: this.questionActuelle.getTraitement()){
            String option;
            if(traitement.getOption().equals("Aucun")){
                option = "";
            }else{
                option = traitement.getOption();
            }
            Button bouton = new Button("Graphique \n"+traitement.getType()+"\n"+option, this.donneImage(traitement.getType()));
            bouton.setOnAction(new ControleurAnalyste(this));
            bouton.setId("Tile"+position);
            tilePane.getChildren().add(bouton);
            ++position;
        }
        if(this.getRole().contains("A")){
            Button btnPlus = new Button("Ajouter un graphique", new ImageView("plus.png"));
            btnPlus.setOnAction(new ControleurAnalyste(this));
            btnPlus.setTooltip(new Tooltip("Ajouter un nouveau graphique"));
            tilePane.getChildren().add(btnPlus);
        }
        ImageView Tableau = new ImageView("tableau.png");
        Button btnBrute = new Button("texte brute",Tableau);
        btnBrute.setOnAction(new ControleurAnalyste(this));
        tilePane.getChildren().add(btnBrute);

    }

    public void miseAJourTilePaneSondeur(){
        tilePane.getChildren().clear();
        if(this.validation){
            tilePane.getChildren().add(this.validerReponse);
        }else{
            int position = 0;
            if(this.questionActuelle.getType().equals("l")){
                Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
                int width  = (int)dimension.getWidth();
                this.zoneTextReponse = new TextArea();
                zoneTextReponse.setPrefWidth(width/2);
                zoneTextReponse.setText(this.questionActuelle.getReponse());
                for(Reponse rep : questionActuelle.getRepondu()){
                    zoneTextReponse.setText(rep.getIntitule());
                }
                tilePane.getChildren().add(zoneTextReponse);
            }else if(this.questionActuelle.getType().equals("n")){
                this.zoneNoteReponse = new TextField();
                zoneNoteReponse.setText(this.questionActuelle.getReponse());
                for(Reponse rep : questionActuelle.getRepondu()){
                    zoneNoteReponse.setText(rep.getIntitule());
                }
                tilePane.getChildren().add(zoneNoteReponse);
            }else if(this.questionActuelle.getType().equals("u")){
                String repChoisit = "";
                for(Reponse rep : questionActuelle.getRepondu()){
                    repChoisit = rep.getIntitule();
                }
                this.questionunique = new ToggleGroup();
                for(String reppos: this.questionActuelle.getReponsePossible()){
                    RadioButton boutton = new RadioButton(reppos);
                    boutton.setId("RD"+position);
                    boutton.setToggleGroup(questionunique);
                    tilePane.getChildren().add(boutton);
                    if(repChoisit.equals(reppos)){
                        this.questionunique.selectToggle(boutton);
                    }else if(repChoisit.equals("")){
                        this.questionunique.selectToggle(boutton);
                        repChoisit = reppos;
                    }
                    ++position;
                }
            }else{
                this.option = new ArrayList<String>();
                for(Reponse rep : questionActuelle.getRepondu()){
                    for(String sousrep :rep.getIntitule().split("; ")){
                        this.option.add(sousrep);
                    }
                }
                for(String reppos: this.questionActuelle.getReponsePossible()){
                    Button bouton = new Button(reppos);
                    bouton.setOnAction(new ControleurBoutonSondeur(this));
                    tilePane.getChildren().add(bouton);
                }
            }
        }
    }

    public void miseAJourTilePaneConcepteur(){
        reponses.getChildren().clear();
        if(this.questionActuelle.getType().equals("l")){
            zoneLibre.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
            Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            int width  = (int)dimension.getWidth();
            reponses.setMaxWidth(width*0.72);
            zoneLibre.setText(this.questionActuelle.getReponse());
            reponses.getChildren().add(zoneLibre);
        }else if(this.questionActuelle.getType().equals("n")){
            zonenote.setText(""+this.questionActuelle.getMaxVal());
            reponses.getChildren().add(zonenote);
        }else{
            for(String reppos: this.questionActuelle.getReponsePossible()){
                reponses.getChildren().add(new TextField(reppos));
            }ImageView imagePlus = new ImageView(new Image("plus.png"));
            imagePlus.setFitWidth(35);
            imagePlus.setFitHeight(35);
            Button ajoutReponse = new Button("Créer réponse", imagePlus);
            ajoutReponse.setPrefSize(170, 100);
            ajoutReponse.setStyle("-fx-padding: 10; -fx-border-style: solid; -fx-border-radius: 10; -fx-focus-color: transparent; -fx-faint-focus-color: transparent; -fx-background-radius :10");
            ajoutReponse.setId("Créer réponse");
            ajoutReponse.setOnAction(new ControleurConcepteur(this));
            reponses.getChildren().add(ajoutReponse);
        }
    }

    public void sauvegardeQuestion() throws NumberFormatException{
        this.questionActuelle.setIntitule(this.concepteurQuestionActuel.getText());
        this.menuChoixQuestion();
        if(this.questionActuelle.getType().equals("n")){
            this.questionActuelle.setMaxVal(Integer.parseInt(this.zonenote.getText()));
        }else if(!this.questionActuelle.getType().equals("l")){
            this.questionActuelle.resetReponsePossible();
            for(Node reppos: reponses.getChildren()){
                if(reppos instanceof TextField){
                    TextField intitulerep = (TextField) reppos;
                    this.questionActuelle.addReponsePossible(intitulerep.getText());
                }
            }
        }
    }

    public void sauvegardeReponse() throws NumberFormatException{
        this.questionActuelle.getRepondu().clear();
        if(this.questionActuelle.getType().equals("l")){
            this.questionActuelle.ajoutRepondu(new Reponse(zoneTextReponse.getText(), 1, null, null, null));
        }else if(this.questionActuelle.getType().equals("n")){
            this.questionActuelle.ajoutRepondu(new Reponse(zoneNoteReponse.getText(), 1, null, null, null));
        }else if(this.questionActuelle.getType().equals("u")){
            RadioButton bouton = (RadioButton) questionunique.getSelectedToggle();
            this.questionActuelle.ajoutRepondu(new Reponse(bouton.getText(), 1, null, null, null));
        }else{
            String rep = "";
            for(String option: this.option){
                rep += option+"; ";
            }
            if(rep.length() !=0){
                rep = rep.substring(0, rep.length()-2);
            }
            this.questionActuelle.ajoutRepondu(new Reponse(rep, 1, null, null, null));
            // for(String reppos: this.questionActuelle.getReponsePossible()){
            //     tilePane.getChildren().add(new Button(reppos));
            // }
        }
    }

    public void miseAJourQuestionConcepteur(){
        this.concepteurQuestionActuel.setText(this.questionActuelle.getIntituler());
        if(this.questionActuelle!=null){
            this.miseAJourQuestion();
            this.setnumQuestion(this.getQuestionnaire().indexOf(this.getQuestionActuelle())+1);
        }else{
            this.setnumQuestion(0);
        }
    }

    public void validerQuestionnaire(){
        try{
            this.bdConcepteur.validation(this.questionnaire.getId());;
        }catch(Exception e){}
    }

    public void sauvegarder(){
        try{
            this.bdConcepteur.sauvegarde(this.questionnaire, this.user);
        }catch(Exception e){}
    }

    public void sauvegarderEntreprise(){
        try{
            this.bdConcepteur.insertion(this.RS.getText(),this.AD1.getText(), this.AD2.getText(), this.CP.getText(), this.V.getText(), this.Tel.getText(), this.Email.getText());
        }catch(Exception e){}
    }

    public boolean getValidation(){return this.validation;}

    public void passerValidation(){this.validation = true;}

    public void sortirValidation(){this.validation = false;}

    public void newSonder(){
        try {
            this.sonde = bdSondeur.selectSonde(this.questionnaire);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void sauvegardeTraitement() throws SQLException{
        this.bdQuestionnaire.sauvegarderAnalyste(this.questionnaire);
    }

    public Sonde getSonde(){return this.sonde;}

    public void actionCliquer(String option){
        if(this.option.contains(option)){
            this.option.remove(option);
        }else{
            this.option.add(option);
        }
    }
}



