import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class FenetreAnalyste extends BorderPane{
    private AppliAllo45 appli;
    private Text intituleQuestion;

    public FenetreAnalyste(AppliAllo45 appli){
        super();
        this.appli = appli;
        this.setPadding(new Insets(20));
        this.intituleQuestion = this.appli.getanalysteIntituleQuestion();
        this.setStyle("-fx-background-color:#e7f2f3;");
        this.setTop(this.banniere());
        this.setCenter(this.analyse());
        this.setRight(this.appli.getMenueDeroulant());
        this.setBottom(this.changeDeQuestion());
    }

    //top de la page de de conception/analyste
    private BorderPane banniere(){
        BorderPane banniere = new BorderPane();
        GridPane info = new GridPane();
        info.add(new Label("Entreprise : "),0,0);
        info.add(this.appli.getentreprise(),1,0);
        info.add(new Label("Population interogée : "),0,1);
        info.add(this.appli.getpanel(),1,1);
        info.setStyle("-fx-padding : 3; -fx-border-width : 4; -fx-border-color : #137b8b; -fx-border-style: solid; -fx-border-radius : 10; -fx-background-color: #f9f7f0; -fx-background-radius: 10;");

        banniere.setLeft(info);
        banniere.setRight(this.appli.getButtonDeconnexion());
        banniere.setPadding(new Insets(0,0,20,0));
        return banniere;
    }

    // centre de la page d'analyse comprenant les outils
    private BorderPane analyse(){
        BorderPane analyse = new BorderPane();
        VBox topAnalyse = new VBox();       
        topAnalyse.setAlignment(Pos.CENTER);
        HBox laQuestion = new HBox();

        Text numQuestion = this.appli.getnumQuestion();
        numQuestion.setStyle("-fx-font-weight:bold;-fx-font-size:1.3em;-fx-padding:4px;");
        numQuestion.setFill(Color.web("#137b8b")); 
        laQuestion.getChildren().add(numQuestion);
        laQuestion.setPadding(new Insets(5));
        
        this.intituleQuestion.setFont(Font.font("Arial", FontPosture.REGULAR, 18.0));
        this.intituleQuestion.setStyle("-fx-padding : 3; -fx-border-style: solid; -fx-border-radius : 10");
        topAnalyse.getChildren().addAll(laQuestion,this.intituleQuestion);
        TilePane lesGraph = this.appli.getCentre();
        analyse.setTop(topAnalyse);
        analyse.setCenter(lesGraph);
        analyse.setPadding(new Insets(20));
        topAnalyse.setPadding(new Insets(20));
        lesGraph.setPadding(new Insets(20));
        analyse.setStyle("-fx-padding : 1; -fx-border-style: solid; -fx-border-width : 5; -fx-border-color : #137b8b;  -fx-border-radius : 20; -fx-background-color: #f9f7f0;-fx-background-radius: 20;");
        return analyse;
    }

    private HBox changeDeQuestion(){
        Image prec = new Image("prec.png");
        Image suiv = new Image("suiv.png");
        ImageView precedent = new ImageView(prec);
        ImageView suivant = new ImageView(suiv);
        precedent.setFitWidth(35);
        precedent.setFitHeight(35);
        suivant.setFitWidth(35);
        suivant.setFitHeight(35);
        Button btnPrec = new Button("Question \nprécedente",precedent);
        btnPrec.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        btnPrec.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        btnPrec.setTextFill(Color.web("#137b8b"));
        btnPrec.setContentDisplay(ContentDisplay.LEFT);
        btnPrec.setPrefWidth(150);

        Button btnSuiv = new Button("Question \nsuivante", suivant);
        btnSuiv.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        btnSuiv.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        btnSuiv.setTextFill(Color.web("#137b8b"));
        btnSuiv.setContentDisplay(ContentDisplay.RIGHT);        
        btnSuiv.setPrefWidth(150);

        btnPrec.setId("prec");
        btnSuiv.setId("suiv");
        btnPrec.setOnAction(new ControleurChangerQuestion(appli));
        btnSuiv.setOnAction(new ControleurChangerQuestion(appli));
        btnPrec.setTooltip(new Tooltip("Allez a la question précédente"));
        btnSuiv.setTooltip(new Tooltip("Allez a la question suivante"));
        HBox changeDeQuestion = new HBox(10);
        changeDeQuestion.getChildren().addAll(btnPrec,btnSuiv);
        changeDeQuestion.setPadding(new Insets(20,0,0,0));
        HBox bas = new HBox();
        HBox retour = new HBox();
        Image home = new Image("home.png");
        ImageView maison = new ImageView(home);
        maison.setFitHeight(35);
        maison.setFitWidth(35);
        Button boutonRetour = new Button("Pour retourner à la \nséléction des sondages",maison);
        boutonRetour.setStyle("-fx-background-color: #fffbf0; -fx-border-width: 4px; -fx-border-color: #137b8b; -fx-border-radius:5; -fx-background-radius:10;");
        boutonRetour.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.REGULAR, 13));
        boutonRetour.setTextFill(Color.web("#137b8b"));
        boutonRetour.setContentDisplay(ContentDisplay.RIGHT);        
        boutonRetour.setPrefWidth(250);

        
        retour.getChildren().add(boutonRetour);
        retour.setPadding(new Insets(20,0,0,0));
        bas.setMaxWidth(Double.MAX_VALUE);
        retour.setAlignment(Pos.CENTER_RIGHT);
        boutonRetour.setOnAction(new ControleurRetour(this.appli));
        HBox.setHgrow(retour, Priority.ALWAYS);
        changeDeQuestion.setAlignment(Pos.CENTER_LEFT);
        bas.getChildren().addAll(changeDeQuestion, retour);
        return bas;
    }
}
