import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BDConcepteur {
	ConnexionMySQL laConnexion;
	Statement st;
    public BDConcepteur(ConnexionMySQL laConnexion){
        this.laConnexion = laConnexion;
    }
    
    public List<String> getNomEntreprise() throws SQLException{
        st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("SELECT raisonSoc from CLIENT");
        List<String> res = new ArrayList<>();
        while(rs.next()){
            res.add(rs.getString("raisonSoc"));
        }
        return res;
    }
    
    public List<String> getPanel() throws SQLException{
        st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("SELECT nomPan from PANEL");
        List<String> res = new ArrayList<>();
        while(rs.next()){
            res.add(rs.getString("nomPan"));
        }
        return res;
    }

    public void sauvegarde(Questionnaire questionnaire, Utilisateur user) throws SQLException{
        st = laConnexion.createStatement();
        int id = questionnaire.getId();
        if(id == 0){
            ResultSet rs = st.executeQuery("SELECT MAX(idQ) FROM QUESTIONNAIRE");
            rs.next();
            id = rs.getInt(1)+1;
            rs = st.executeQuery("SELECT idPan from PANEL where nomPan='"+questionnaire.getPanel()+"'");
            rs.next();
            int idpan = rs.getInt(1);
            rs = st.executeQuery("SELECT numC from CLIENT where raisonSoc='"+questionnaire.getEntreprise()+"'");
            rs.next();
            int identreprise = rs.getInt(1);
            st.executeUpdate("INSERT INTO QUESTIONNAIRE VALUES ("+id+",'"+questionnaire.getNom()+
            "','C',"+identreprise+","+user.getId()+","+idpan+")");
        }
        st.executeUpdate("DELETE from VALPOSSIBLE where idQ='"+questionnaire.getId()+"'");
        st.executeUpdate("DELETE from QUESTION where idQ='"+questionnaire.getId()+"'");
        for(Question quest: questionnaire.getQuestion()){
            int indice = questionnaire.indexOf(quest);
            if(quest.getType().equals("u")|| quest.getType().equals("l")){
                st.executeUpdate("INSERT INTO QUESTION VALUES ("+id+","+indice+",'"+quest.getIntituler()+"',NULL,'"+quest.getType()+"')");
            }else{
                st.executeUpdate("INSERT INTO QUESTION VALUES ("+id+","+indice+",'"+quest.getIntituler()+"',"+quest.getMaxVal()+",'"+quest.getType()+"')");
            }
            if(quest.getType().equals("u")|| quest.getType().equals("m") || quest.getType().equals("c")){
                int indiceval = 1;
                for(String reppos:quest.getReponsePossible()){
                    st.executeUpdate("INSERT INTO VALPOSSIBLE VALUES ("+id+","+indice+","+indiceval+",'"+reppos+"')");
                    indiceval++;
                }
            }
        }
    }
    public void validation(int id) throws SQLException{
        st = laConnexion.createStatement();
        st.executeUpdate("UPDATE QUESTIONNAIRE SET Etat='S' WHERE idQ='"+id+"'");
    }

    public void insertion(String raisonSoc, String adresse1, String adresse2, String CodePostal, String Ville, String Telephone, String email)throws SQLException{
        st = laConnexion.createStatement();
        ResultSet rs = st.executeQuery("SELECT MAX(numC) FROM CLIENT");
        rs.next();
        String adr2="";
        if(adresse2.equals("")){
            adr2 = "NULL";
        }else{
            adr2 = "'"+adresse2+"'";
        }
        st.executeUpdate("INSERT INTO CLIENT VALUES("+(rs.getInt(1)+1)+",'"+raisonSoc+"','"+adresse1+"',"+adr2+",'"+Integer.parseInt(CodePostal)+"','"+Ville+"','"+Telephone+"','"+email+"')");
    }
}
