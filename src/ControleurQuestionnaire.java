import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurQuestionnaire implements EventHandler<ActionEvent>{
    private AppliAllo45 appli;
    private BDQuestionnaire bd;
    public ControleurQuestionnaire(AppliAllo45 appli, BDQuestionnaire bd){
        this.appli = appli;
        this.bd = bd;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        Button boutton = (Button) actionEvent.getSource();
        String id = boutton.getId();
        String role = appli.getRole();
        if(role.equals("Concepteur")){
            try{
                appli.setQuestionnaire(bd.getQuestionnaireConcepteur(id));
                appli.modeConcepteur(); 
            }catch(Exception e){}
        }else{
            try{
                if(role.contains("A") || role.equals("Entreprise")){
                    appli.setQuestionnaire(bd.getQuestionnaireAnalyste(id));
                }else{
                    appli.setQuestionnaire(bd.getQuestionnaireConcepteur(id));
                    appli.newSonder();
                }
                appli.modeAnalyste(); 
            }catch(Exception e){
                System.out.println(e);
            }
        }
        if(this.appli.getQuestionnaire().size()>0){
        this.appli.setQuestionActuelle(this.appli.getQuestionnaire().get(0));
        }
        appli.miseAJourChoixSondage();
    }
}
