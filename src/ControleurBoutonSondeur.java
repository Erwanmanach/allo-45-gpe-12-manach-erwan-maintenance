import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class ControleurBoutonSondeur implements EventHandler<ActionEvent>{
    private AppliAllo45 appli;
    public ControleurBoutonSondeur(AppliAllo45 appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent){
        Button boutton = (Button) actionEvent.getSource();
        String nom = boutton.getText();
        this.appli.actionCliquer(nom);
    }
}
