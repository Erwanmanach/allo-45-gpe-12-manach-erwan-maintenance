import java.util.ArrayList;
import java.util.List;

public class Questionnaire extends ArrayList<Question>{
    private String nom, entreprise, panel;
    private int id;
    public Questionnaire(String nom){
        super();
        this.nom = nom;
        this.panel = "";
        this.entreprise = "";
        this.id = 0;
    }
    public Questionnaire(String nom,String entreprise,String panel){
        super();
        this.nom = nom;
        this.panel = panel;
        this.entreprise = entreprise;
        this.id = 0;
    }
    public Questionnaire(String nom,String entreprise,String panel,int id){
        super();
        this.nom = nom;
        this.panel = panel;
        this.entreprise = entreprise;
        this.id = id;
    }
    public String getEntreprise(){return this.entreprise;}
    public String getPanel(){return this.panel;}
    /**
     * Retourne la liste des questions
     * @return la listes des questions
     */
    public List<Question> getQuestion(){return this;}
    /**
     * Ajoute une question aux questionnaire
     * @param q la question à ajouter
     */
    public void ajouterQuestion(Question q){this.add(q);}
    /**
     * Supprime la qestion
     * @param q la question à supprimer
     * @throws QuestionnotFind si la question n'est pas dans le questionnaire
     */
    public void supprimerQuestion(Question q) throws QuestionnotFind{
        int indice = this.indexOf(q);
        if(indice == -1){
            throw new QuestionnotFind();
        }else{
            this.remove(indice);
        }
    }
    public String getNom(){return this.nom;}
    public int numQ(Question q){
        return this.indexOf(q);
    }
    public int getId(){return this.id;}
}